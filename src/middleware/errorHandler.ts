import { Middleware, KoaMiddlewareInterface } from 'routing-controllers';

import * as Boom from 'boom'

@Middleware({ type: 'before' })
export class CustomErrorHandler implements KoaMiddlewareInterface {
  public async use(context: any, next: (err?: any) => Promise<any>) {
    try {
      await next();
    } catch (e) {      
      if (e.code && e.code === 11000) {       
        // console.log(e);
        
        e = Boom.conflict('Db already exist')    
      }   
      let payload = e;
      if (e.errors) {
        const values: any[] = Object.values(e.errors)
        if (values.length && values[0].reason.isBoom) {
          const boomError: Boom<any> = values[0].reason
          payload = boomError.output.payload;
          payload.data = boomError.data;
        }
      
        if (e.errors.length) {
          e.errors.forEach((item: any) => {
            payload.message = Object.values(item.constraints);
            payload.status = 405;
          });
        }

      }

      if (e.isBoom) {
        // console.log(e);

        payload = e.output.payload;
        payload.data = e.data;
      } else {
        console.error("UNHANDLED ERROR", e);

      }
      context.status = payload.status || payload.statusCode || 500;
      context.body = payload;
    }
  }
}