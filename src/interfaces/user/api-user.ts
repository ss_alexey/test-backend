export interface RegisterUser {
    firstName: string;
    email?: string;
    password: string;
    confirmPassword: string;
    phone?: string;
}