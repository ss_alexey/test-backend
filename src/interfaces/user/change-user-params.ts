export interface ChangeUserAvatar {
    userId: string,
    imgUrl: string
}