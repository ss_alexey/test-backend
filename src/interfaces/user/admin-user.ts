export interface RegisterAdminUser {
    firstName: string;
    email: string;
    password: string;
    confirmPassword: string
}