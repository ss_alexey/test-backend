export interface UserParams {
    id: string
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmPassword: string;
    phone: string;
    lastVisit: Date;

}
export interface UserParamsJson {
    id: string,
    firstName: string;
    lastName: string;
    email: string;
    phone: string | null
}
