export interface UploadFile {
    buffer: Buffer
    originalname: string
    fieldName: string
    encoding: '7bit'
    mimetype: string
}