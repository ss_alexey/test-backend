

export interface DishRegister {
    category: any
    name: string
    price: number
    img?: string
    description: string
}
export type DishParams = DishRegister & {
    id: string
}