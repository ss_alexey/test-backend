export interface VisitRegister {
    establishmentId: string,
    userId: string,
}
export type VisitParams = VisitRegister & {
    id: string
}