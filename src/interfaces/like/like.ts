export interface SaveLike {
    parent: string;
    owner: string;
    value: boolean;
}