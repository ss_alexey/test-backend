export interface SaveRate {
    value: number,
    target: string,
    owner: string
}