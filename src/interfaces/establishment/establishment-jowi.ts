export interface JowiEstablishmentResponse {
    status: number,
    restaurants: any[]
}
export interface JowiEstablishment {
    id: string,
    title: string,
    tax: number,
    is_course_tax: boolean,
    phone_numbers: string[],
    website: string,
    work_time: string,
    restaurant_type: string,
    live_music: boolean,
    wi_fi_zone: boolean,
    smoking_zone: boolean,
    non_smoking_zone: boolean,
    veggie_dishes: boolean,
    open_halls: boolean,
    room_for_children: boolean,
    currency_of_payment: string[],
    payment_by_card: boolean,
    average_amount_bill: number,
    address: string,
    facebook_page: string,
    twitter_page: string,
    description: string,
    longitude: number,
    latitude: number,
    country: string,
    city: string,
    online_order: boolean,
    online_reserve: boolean,
    delivery_price: number,
    delivery_time: string,
    work_timetable: any[],
    images: JowiImages[]
}
interface JowiImages {
    url: string,
    description: string,
    order: number
}
