export interface EstablishmentFeature {
    value: string,
    key: string
}