

export interface EstablishmentInformation {
    workTime: {
        [key: string]: {
            open_time: string,
            close_time: string,
            day_off: boolean,
        }
    };
    feature: {
        selected: string[],
        all: string[]
    }
}
