export interface AddEstablishmentUserPhoto {
    userId: string,
    imgUrl: string,
    estModelDetalId: string
}