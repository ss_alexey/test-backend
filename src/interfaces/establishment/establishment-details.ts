interface WorkTimeTable {
    day_code: number,
    open_time: string,
    close_time: string,
    day_off: boolean,
    timetable_code: number
}
interface EstablishmentImage {
    url: string,
    description: string,
    order: number
}

export interface Menu {
    title: string,
    id: string,
    dishes: Dish[]
}
export interface Dish {
    price: number,
    name: string,
    img: string,
    description: string,
    id: string
}
export interface EstablishmentDetails {
    userPhotos: { owner: string, imgUrl: string }[],
    photos: string[],
    description: string,
    id: string,
    menu: Menu[]
}
