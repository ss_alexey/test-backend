import { EstablishmentRegister } from './establishment-register'

export type EstablishmentParams = EstablishmentRegister & {
    information: string,
    details: string,
    rating: {
        lastVote: number,
        average: number,
        modelId: string,
        voteCount: number
    },
    id: string
}
