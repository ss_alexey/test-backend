export interface EstablishmentAdress {
    number: Number,
    street: String,
    city: String,
    country: String,
}

export interface EstablishmentCoordinate {
    latitude: number,
    longitude: number
}

export interface EstablishmentRegister {
    location? : any
    coordinate: EstablishmentCoordinate,
    title: String,
    type: String,
    adress: EstablishmentAdress,
    phone: string,
    image?: string,
    details: string
}