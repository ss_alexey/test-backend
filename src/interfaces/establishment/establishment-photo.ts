export interface AddEstablishmentPhoto {
    imgUrl: string,
    establishmentDetailModelId: string
}