import { JoxiDish } from './dish'

export interface JoxiMenu {
    status: number;
    categories: JoxiCategory[]
}
export interface JoxiCategory {
    title: string;
    courses: JoxiDish[]
}