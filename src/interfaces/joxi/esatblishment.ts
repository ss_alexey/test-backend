import { Decimal128, Timestamp } from "bson";

export interface JoxiEstablishment {
    id: string;
    title: string;
    phone_numbers: string;
    website: string;
    restaurant_type: string;
    live_music: string;
    wi_fi_zone: string;
    smoking_zone: string;
    non_smoking_zone: string;
    veggie_dishes: string;
    open_halls: string;
    room_for_children: string;
    currency_of_payment: string;
    payment_by_card: string;
    average_amount_bill: number;
    address: string;
    facebook_page: string;
    twitter_page: string;
    description: string;
    longitude: Decimal128
    latitude: Decimal128
    country: string;
    city: string;
    online_reserve: boolean
    delivery_price: number
    delivery_time: Timestamp
    work_timetable: {day_code: number, open_time: string, day_off: boolean, timetable_code: number}[]
    timetable_code: 1 | 2 | 3
    images: { url: string, description: string, order: number }[]
}