export interface JoxiDish {
    id: string
    title: string;
    price: number;
    price_for_online_order: number;
    // is_exception === Блюдо исключение, если исключение то не считается налог на блюдо, налог на счет, обслуживание и скидка на счет
    is_exception: boolean
    tax: number;
    online_order: boolean;
    is_vegetarian: boolean
    cooking_time: string;
    caloric_content: string;
    description: string;
    image_url: string
}
