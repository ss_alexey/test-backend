

export interface RegisterComment {
    text: string,
    parent: string,
    owner: string
}
export type CommentParams = RegisterComment & {
    id: string
}