import 'reflect-metadata'; // this shim is required
import { useKoaServer, Action, HttpError, createKoaServer } from 'routing-controllers';
import * as Koa from 'koa';
import * as mongoose from 'mongoose'

import * as Boom from 'boom'
// import * as cors from '@koa/cors'
import { AuthService } from './helpers';
// import { userSchema } from './db/schemas';
// import { UserModel } from './db/models';
import { testAccess } from './validators/rbac'


export let connection: mongoose.Connection;

export const startServer = async (port: number | string, dbUrl: string) => {
    const app: Koa = new Koa();
    try {
        mongoose.set('useFindAndModify', false);
        connection = await mongoose.createConnection(dbUrl, { useCreateIndex: true, useNewUrlParser: true, reconnectTries: 5, reconnectInterval: 500, autoIndex: true })
        console.log('CONNECTED TO DB');
    } catch (e) {
        console.error('DB ERROR', e);
    }
    // app.use(cors())
    await useKoaServer(app, {
        classTransformer: true,
        validation: true,
        authorizationChecker: authChecker,
        routePrefix: '',
        defaultErrorHandler: false,
        middlewares: [__dirname + '/middleware/**/*.ts'],
        controllers: [__dirname + '/controllers/**/*.ts']
    });


    console.log(`Server running on port ${port}`);
    return app.listen(port);
};
const authChecker = async (action: Action, roles: string[]): Promise<boolean> => {


    const token = action.request.headers["authorization"];
    if (!token) {
        throw Boom.unauthorized()
    }
    try {
        await AuthService.tokenDecode(token)
    } catch (e) {
        throw Boom.unauthorized(e)
    }
    // await testAccess(action.request, token)

    return true
}