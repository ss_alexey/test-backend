import { randomIndex } from '../randomNumbers'

export const sites = [
    'https://www.barcrenn.com/',
    'https://www.jeffreysgrocery.com/',
    'https://www.ililinyc.com/',
    'https://www.bobonyc.com/',
    'http://resto-elephant.com/',
    'https://4rsmokehouse.com/',
    'https://www.risotteriamelottinyc.com/',
    'https://www.theeddynyc.com/',
    'http://www.brassunion.com/',
    'https://www.quay.com.au/',
    'http://junctionmoama.com.au/',
    'http://www.arbor-restaurant.co.uk/',
    'https://www.restaurantebaobab.com/',
    'http://mercertavern.com/',
    'https://www.whitmansnyc.com/'

]
export const getRandomSite = () => {
    return sites[randomIndex(sites.length)]
}