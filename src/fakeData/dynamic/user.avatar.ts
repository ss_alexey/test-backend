import { users } from '../savedData'
import { avatars } from '../static'
import { randomIndex } from '../randomNumbers'
import { ChangeUserAvatar } from '@interfaces'


export const generateUserAvatars = async (): Promise<ChangeUserAvatar[]> => {
    return users.map(user => {
        return { userId: user._id, imgUrl: avatars[randomIndex(avatars.length)] }
    })
}