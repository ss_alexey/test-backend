import { AddEstablishmentUserPhoto } from '@interfaces'
import { establishments, users } from '../savedData'
import { randomIndex } from '../randomNumbers'
import { vistorPhotos } from '../static'

export const generateEstUserPhotos = async (count: number): Promise<AddEstablishmentUserPhoto[]> => {
    const arr: AddEstablishmentUserPhoto[] = []
    for (let i = 0; i <= count; i++) {

        const comment: AddEstablishmentUserPhoto = {
            userId: users[randomIndex(users.length)]._id,
            estModelDetalId: establishments[randomIndex(establishments.length)].details,
            imgUrl: vistorPhotos[randomIndex(vistorPhotos.length)]
        }
        arr.push(comment)
    }
    return arr
}