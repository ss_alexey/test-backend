import { AddEstablishmentPhoto } from '@interfaces'
import { establishments, users } from '../savedData'
import { randomIndex } from '../randomNumbers'
import { establishmentImages } from '../static'

export const generateEstPhotos = async (count: number): Promise<AddEstablishmentPhoto[]> => {
    const arr: AddEstablishmentPhoto[] = []
    for (let i = 0; i <= count; i++) {

        const estPhoto: AddEstablishmentPhoto = {
            establishmentDetailModelId: establishments[randomIndex(establishments.length)].details,
            imgUrl: establishmentImages[randomIndex(establishmentImages.length)]
        }
        arr.push(estPhoto)
    }
    return arr
}