import { Establishment, User, Comment } from '../db/models'

import { users, establishments } from './savedData'

import { randomIndex, randomNumber } from './randomNumbers'
import { comments } from './savedData'
export interface FakeLike {
    parent: string;
    owner: string;
    value: boolean;
}

export const generateLikes = async (count: number): Promise<FakeLike[]> => {

    const arr = []
    for (let i = 0; i <= count; i++) {

        const comment: FakeLike = {
            value: !!(randomNumber(2, 0)),
            parent: comments[randomIndex(comments.length)]._id,
            owner: users[randomIndex(users.length)]._id
        }
        arr.push(comment)
    }
    return arr
}