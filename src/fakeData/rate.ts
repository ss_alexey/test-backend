import { Establishment, User, Comment } from '../db/models'

import { users, establishments } from './savedData'

import { randomIndex, randomNumber } from './randomNumbers'
export interface FakeRate {
    value: number,
    target: string,
    owner: string
}

export const generateRates = async (count: number, maxRate: number, minRate: number): Promise<FakeRate[]> => {

    const arr = []
    for (let i = 0; i <= count; i++) {

        const comment: FakeRate = {
            value: randomNumber(maxRate || 10, minRate || 1),
            target: establishments[randomIndex(establishments.length)].rating.modelId,
            owner: users[randomIndex(users.length)]._id
        }
        arr.push(comment)
    }
    return arr
}