export let users: any[]
export let establishments: any[]
export let comments: any[]
import { User, Establishment, Rate, Comment } from '../db/models'

export const generateSavedData = async () => {
    users = (await User.find())
    establishments = (await Establishment.find())
    comments = (await Comment.find())
}
export const updateUsers = async () => {
    users = (await User.find())
}
export const uodateEstablishments = async () => {
    establishments = (await Establishment.find())
}
export const updateComments = async () => {
    comments = (await Comment.find())
}