import { generateSavedData, updateUsers, updateComments } from './savedData'
import { generateFakeUsers } from './user'
import {
    saveUser,
    saveComment,
    saveLikeDb,
    saveVisit,
    saveRateDb,
    getEstablishments,
    addEstablishmentPhoto,
    addEstablishmentUserPhotoDb,
    changeUserAvatar
} from '../db/db-services'
import { generateComments } from './comment';
import { generateLikes } from './like'
import { generateVisits } from './visit'
import { generateRates } from './rate'
import { generateEstPhotos, generateEstUserPhotos, generateUserAvatars } from './dynamic'

import * as Boom from 'boom'
import { Establishment, EstablishmentPlacesModel, EstablishmentTableModel } from '../db/models';


interface RandomData {
    userCount: number
    commentCount: number
    likeCount: number
    rateCount: number
    visitCount: number
    userPhotoCount: number
    establishmentPhotoCount: number
    assignUserAvatars: boolean
    rating: {
        max: number,
        min: number
    },
    generateTables: boolean
}
export const saveFakeData = async (data: RandomData) => {
    try {
        if (!(await getEstablishments()).length) {
            throw Boom.notFound('no establishment records')
        }
        const { userCount, likeCount, commentCount, visitCount, rateCount, userPhotoCount, establishmentPhotoCount, assignUserAvatars, generateTables } = data
        await generateSavedData()
        if (userCount) {
            const users = await generateFakeUsers(userCount)
            await Promise.all(users.map(async user => {
                return (await saveUser(user))
            }))
            await updateUsers()
        }
        if (commentCount) {
            const comments = await generateComments(commentCount)
            await Promise.all(comments.map(async comment => {
                return (await saveComment(comment))
            }))
            await updateComments()
        }
        if (likeCount) {
            const likes = await generateLikes(likeCount)
            await Promise.all(likes.map(async like => {
                return (await saveLikeDb(like))
            }))
        }
        if (visitCount) {
            const visits = await generateVisits(visitCount)
            await Promise.all(visits.map(async visit => {
                return (await saveVisit(visit))
            }))
        }
        if (rateCount) {
            const rates = await generateRates(rateCount, data.rating && data.rating.max, data.rating && data.rating.min)
            await Promise.all(rates.map(async rate => {
                return (await saveRateDb(rate))
            }))
        }
        if (establishmentPhotoCount) {
            const photos = await generateEstPhotos(establishmentPhotoCount)
            await Promise.all(photos.map(async photo => {
                return (await addEstablishmentPhoto(photo))
            }))
        }
        if (userPhotoCount) {
            const photos = await generateEstUserPhotos(userPhotoCount)
            await Promise.all(photos.map(async photo => {
                return (await addEstablishmentUserPhotoDb(photo))
            }))
        }
        if (assignUserAvatars) {
            const avatars = await generateUserAvatars()
            await Promise.all(avatars.map(async avatar => {
                return (await changeUserAvatar(avatar))
            }))
        }
        if (generateTables) {
            const est = (await Establishment.find())
            await Promise.all(est.map(async (est: any) => {
                const placeModelId = (await new EstablishmentPlacesModel({ title: 'какой-то зал', openair: false }).save()).id
                await Establishment.findByIdAndUpdate(est.id, { $push: { places: placeModelId } })
                for (let i = 0; i <= 5; i++) {
                    const tableId = (await new EstablishmentTableModel({ place_count: 5, establishmentModelId: est.id, placeModelId: placeModelId }).save()).id
                    await EstablishmentPlacesModel.findByIdAndUpdate(placeModelId, { $push: { tables: tableId } })
                }
            }))
         
        }
    } catch (error) {
        console.log();

        throw error
    }


}