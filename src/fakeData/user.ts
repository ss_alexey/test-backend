import * as mongoose from 'mongoose'
import * as dummy from 'mongoose-dummy'

const ignoredFields = ['_id', 'created_at', '__v', /detail.*_info/];
let schemaDefinition = new mongoose.Schema({
    firstName: String,
    password: String,
});

interface FakeUser {
    password: string,
    firstName: string,
    confirmPassword: string,
    phone: string
}

let phone = 665349846
let model = mongoose.model('Student', schemaDefinition);
export const generateFakeUsers = async (count: number): Promise<FakeUser[]> => {
    const arr: any[] = []
    for (let i = 0; i <= count; i++) {
        const test = dummy(model, {
            ignore: ignoredFields,
            returnDate: true
        })
        phone++


        const user = { ...test, confirmPassword: test.password, phone: `+380${phone}` }


        arr.push(user)
    }
    return arr
}
