

import { users, establishments } from './savedData'

import { randomIndex } from './randomNumbers'
interface FakeComment {
    text: string,
    parent: string,
    owner: string
}
import * as mongoose from 'mongoose'
import * as dummy from 'mongoose-dummy'
const ignoredFields = ['_id', 'created_at', '__v', /detail.*_info/];
let schemaDefinition = new mongoose.Schema({

    data: {
        type: Object,
        default: null
    }

});


let model = mongoose.model('Comment', schemaDefinition);
export const generateComments = async (count: number): Promise<FakeComment[]> => {

    const arr = []
    for (let i = 0; i <= count; i++) {
        const test = dummy(model, {
            ignore: ignoredFields,
            returnDate: true
        })
        const result = test.data[Object.keys(test.data)[0]].posts[0].paragraph
        const comment: FakeComment = {
            text: result,
            parent: establishments[randomIndex(establishments.length)]._id,
            owner: users[randomIndex(users.length)]._id
        }
        arr.push(comment)
    }
    return arr

}