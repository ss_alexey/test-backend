import { establishments, users } from './savedData'
import { randomIndex } from './randomNumbers';

export interface VisitRegister {
    establishmentId: string,
    userId: string,
}

export const generateVisits = async (count: number): Promise<VisitRegister[]> => {
    const arr = []
    for (let i = 0; i <= count; i++) {

        const visit: VisitRegister = {
            establishmentId: establishments[randomIndex(establishments.length)]._id,
            userId: users[randomIndex(users.length)]._id
        }
        arr.push(visit)
    }
    return arr
}