import {
  RegisterUser,
  LoginUser,
} from '../interfaces';
import { validateUserRegister, validateLogin } from '../validators';
import {
  findUserByPhone,
  updateUserLastVisit, saveUser, findUserById, findUserByEmail
} from '../db/db-services';
import { AuthService, generatePin, makeTempUser, checkUserPin, updateTempUser } from '../helpers';
import { sendSms } from './twillio';

const twoFactor = true

type Roles = 'apiUser' | 'socialUser' | 'estOwner' | 'register' | 'login';

type ValidationRoles = Roles[];
export const userRegister = async (
  user: RegisterUser,
  validGroup?: ValidationRoles
) => {
  await validateUserRegister(user, validGroup || []);
  if (twoFactor) {
    return (await makeTemporaryUser(user))
  }
  const newUser = await saveUser(user);
  return newUser.toJSON();
};


export const loginUser = async (
  user: LoginUser
): Promise<{ token: string }> => {
  try {
    const dbUser = await findUserByPhone(user.phone);
    await validateLogin(dbUser, user);
    await updateUserLastVisit(dbUser.id);
    return { token: AuthService.tokenEncode(dbUser.id) };
  } catch (error) {
    throw error;
  }
};

export const makeTemporaryUser = async (user: RegisterUser) => {
  const pin = await generatePin()
  await sendSms(user.phone, pin)
  await makeTempUser(user, pin)
  return { pin }
}
export const generateNewPin = async (phone: any) => {
  try {
    const pin = await generatePin()
    await sendSms(phone, pin)
    await updateTempUser(phone, pin)
    return { pin }
  } catch (error) {
    throw error
  }

}
export const confirmPhone = async (phone: string, pin: string) => {
  const user = await checkUserPin(phone, pin)
  return { token: (await AuthService.tokenEncode((await saveUser(user)).id)) }
  // return (await saveUser(user))
}
// export const regiterApiUser = async (user: RegisterUser): Promise<{ token: string }> => {
//     try {
//         await validateUserRegister(user)
//         const id = (await registerUser(user)).id
//         const token = await AuthService.tokenEncode(id)
//         return { token: token }

//     } catch (error) {
//         throw error
//     }
// }

export const temporaryGoogleLogin = async (token: string) => {

  // const user = await getGoogleUserFromCode(token)
  // console.log(AuthService.decodeJwtToken('eyJhbGciOiJSUzI1NiIsImtpZCI6ImYyNGQ2YTE5MzA2NjljYjc1ZjE5NzBkOGI3NTRhYTE5M2YwZDkzMWYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIzMzg4NTg5ODY3ODItNnJvcG5vZXJhbnVkbWFmZGJhYnEyMm9pbG12MDIxb3AuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIzMzg4NTg5ODY3ODItNnJvcG5vZXJhbnVkbWFmZGJhYnEyMm9pbG12MDIxb3AuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTI0Nzg0NzkxNjYyMzk1MTA5NjMiLCJlbWFpbCI6InBhdmxvdi5zZXJnZXkuYWxleGFuZHJAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiJPamVST1ltdkhJRHcwclY0RTJ4Ti1BIiwibmFtZSI6ItCh0LXRgNCz0LXQuSDQn9Cw0LLQu9C-0LIiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy11UTIwVGVFMjVOZy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JmN1BXVjNab1RpMXpwT3k0N1hIOE9qaERrNjBnL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiLQodC10YDQs9C10LkiLCJmYW1pbHlfbmFtZSI6ItCf0LDQstC70L7QsiIsImxvY2FsZSI6InVrIiwiaWF0IjoxNTUxMjY4MTA2LCJleHAiOjE1NTEyNzE3MDZ9.HmRPJOJKq2RZAIhRUij1WQ4THtqbV5OvZkJHHs99zHmqnKprNiHKAZo35aVX7WKR1muxrZQzeTJYx3nV92fJpFQaJI5XtoKuyr3ysvUtZ23SRPyDFNVTZiErhgyLKmfBOKPaj1M8M_rEgCTg5yed_684HOGUFfiLSO63x9GzDDWCHYb7halgYJ--TOmOSm-xa5USqq7Yp5huASsqAROzYwRfcz3dFFKRSvUC9CmwyvVvnl0wW0s-wA6_8aQg6uRYCObM7aYXm425urPpR3VzrCT7gff22KXvheHR45sBVGv9ekJ1xPkwYXDB0snbMrvgirc6num_hmW6F-frcfA6Kg'));
  const test = AuthService.decodeJwtToken(token)
  // console.log(test);

  const { given_name, family_name, email, locale, picture } = AuthService.decodeJwtToken(token)
  const user = { firstName: given_name, email: email, avatar: picture }
  // await validateGoogleUser(user)
  let userId: string
  try {
    userId = (await findUserByEmail(user.email)).id
  } catch (e) {
    userId = (await saveUser(user)).id
  }
  await updateUserLastVisit(userId)
  // return user
  return AuthService.tokenEncode(userId)
}
export const loginFaceBookUser = async (user: any) => {
  let userId: string 
  try {
    userId = (await findUserByEmail(user.email)).id
  } catch (e) {
    const newUser = {firstName: user.name, email: user.email, avatar: user.url}  
    userId = (await saveUser(newUser)).id
  }
  await updateUserLastVisit(userId)
  return AuthService.tokenEncode(userId)
}
// export const loginGoogleUser = async (params: { code: string, scope: string }) => {
//     const user = await getGoogleUserFromCode(params.code)
//     await validateGoogleUser(user)
//     let userId: string
//     try {
//         userId = (await findUserIdByEmail(user.email))
//     } catch (e) {
//         userId = (await registerGoogleUser(user)).id
//     }
//     await updateUserLastVisit(userId)
//     return AuthService.tokenEncode(userId)

// }
// const registerGoogleUser = async (user: GoogleUserRegister) => {
//     await validateGoogleUser(user)
//     return (await registerUser(user))
// }

export const getUserByToken = async (token: string) => {
  const { id } = AuthService.tokenDecode(token)
  return (await findUserById(id)).toJSON()
}
// export const getById = async (id: string) => {
//     return (await getUserById(id))
// }
