
import { DishRegister, UploadFile } from '../../../../interfaces'
import { validateDishRegister, validateFileIsImage } from '../../../../validators'
import { saveDishToDb, saveFileToDb } from '../../../../db/db-services'
import { createImageUrl } from '../../../../helpers'



export const saveDish = async (dish: DishRegister, file?: UploadFile) => {
    try {
        if (file) {
            await validateFileIsImage(file)
            dish.img = (await createImageUrl((await saveFileToDb(file))._id))
        }
        await validateDishRegister(dish)
        return (await saveDishToDb(dish))
    } catch (error) {
        throw error
    }
}