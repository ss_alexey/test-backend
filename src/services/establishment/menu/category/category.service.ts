import { DishCatergoryRegister } from '../../../../interfaces'
import { validateDishCategoryRegister } from '../../../../validators'
import { saveDishCategoryToDb} from '../../../../db/db-services'


export const saveDishCategory = async (category: DishCatergoryRegister) => {
    try {
        await validateDishCategoryRegister(category)
        await saveDishCategoryToDb(category)
        return { success: true }
    } catch (error) {
        throw error
    }
}