import { getEstablishments, getDbEstablishmentById } from '../../db/db-services'
import { AuthService } from '../../helpers'
import { EstablishmentParams, EstablishmentDetails } from '@interfaces';
import { validateEstablishmentUpdate } from '../../validators';
import { EstablishmentDetailsModel } from '../../db/models'

export const getAllEstablishments = async (token?: string, limit?: number) => {
    try {
        const userId = token ? await AuthService.getIdFromToken(token) : null
        return (await getEstablishments(userId, limit|| null))
    } catch (error) {
        throw error
    }
}
export const getEstablishmentById = async (id: string) => {
    try {
        return getDbEstablishmentById(id)
    } catch (error) {
        throw error
    }
}
export const updateEstablishmentParams = async (estParams: EstablishmentDetails, paramId: string) => {
    await validateEstablishmentUpdate(estParams)
    const result = await EstablishmentDetailsModel.findByIdAndUpdate(paramId, estParams)  
    return result
}