import { EstablishmentRegister, UploadFile, EstablishmentCoordinate } from "../../interfaces";
import { validateEstablishmentRegister, validateFileIsImage } from '../../validators'
import { saveEstablishment, saveFileToDb } from '../../db/db-services'
import * as Boom from 'boom'
import { createImageUrl } from "../../helpers";

// LEFT
// 47.850086
// 35.087995

// RIGHT
// 47.847505
// 35.297404

// TOP
// 47.913603
// 35.177906

// BOT
// 47.784129
// 35.178161
const getRan = (min: number, max: number) => {
    return +(Math.random() * (max - min) + min).toFixed(6);
}
const getRandomCoordinates = (): EstablishmentCoordinate => {
    return { latitude: getRan(47.784129, 47.913603), longitude: getRan(35.087995, 35.297404) }
}
export const registerEstablishment = async (est: EstablishmentRegister, file: UploadFile) => {
    if (!file) {
        throw Boom.notAcceptable('image required')
    }
    try {
        await validateFileIsImage(file)
        est.image = (await createImageUrl((await saveFileToDb(file))._id))
        if (est.type !== 'coffee' && est.type !== 'fast_food' && est.type !== 'restaraunt' && est.type !== 'shushi_bar') {
            throw Boom.notAcceptable('avilable just  shushi_bar coffee restaraunt fast_food')
        }
        if (!est.coordinate) {
            const coor = getRandomCoordinates()
            est.coordinate = coor
            est.location = {
                type: 'Point',
                coordinates: [coor.longitude, coor.latitude]
            }
        }
        await validateEstablishmentRegister(est)
        await saveEstablishment(est)
    } catch (error) {
        throw error
    }

    return { success: true }
}