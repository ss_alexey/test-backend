import { RegisterComment } from '@interfaces'
import { validateCommentRegister } from '../../validators'
import { AuthService } from '../../helpers'
import { saveComment, findUserById } from '../../db/db-services'

import * as Boom from 'boom'


export const registerComment = async (comment: RegisterComment, token: string) => {
    try {
        const userId =  await AuthService.getIdFromToken(token)  
        if(!await findUserById(userId)){
            throw Boom.unauthorized()
        }
        comment.owner =  userId
        await validateCommentRegister(comment)
        const result = await saveComment(comment)
        return result
    } catch (error) {
        throw error
    }
    // comment.owner = await AuthService.getIdFromToken(token)
    // console.log(typeof comment.value);
    // if (typeof comment.value === 'string') {
    //     await validateCommentRegister(comment, ['text'])
    //     return (await saveComment(comment, 'establishment'))
    // }
    // if (typeof comment.value === 'boolean') {
    //     await validateCommentRegister(comment, ['like'])
    //     return (await saveComment(comment, 'comment'))
    // }
    // const result = await saveComment(comment, 'establishment')
    // return 'result'
}