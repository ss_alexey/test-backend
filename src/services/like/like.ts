import { validateLikeRegister } from '../../validators'
import { SaveLike } from '@interfaces'
import { saveLikeDb, findUserById, findDishById } from '../../db/db-services'
import { AuthService } from '../../helpers';
import * as Boom from 'boom'

export const saveLike = async (like: SaveLike, token: string) => {
    try {
        const userId = await AuthService.getIdFromToken(token)
        if (!await findDishById) {
            throw Boom.notFound('no such user')
        }
        like.owner = userId
        await validateLikeRegister(like)
        return (await saveLikeDb(like))
    } catch (error) {
        throw error
    }
}