import { google } from 'googleapis'
import { OAuth2Client } from 'google-auth-library';
import { AuthService } from '../../helpers'
import { GoogleUserRegister } from '../../interfaces';


const SCOPES = ['https://www.googleapis.com/auth/userinfo.email'];
const client_id = '338858986782-qec748q45crt8b4qcccev10322q8fv88.apps.googleusercontent.com'
const client_secret = 'wV1yKBog9ecVxqQUhijx8KDt'
const redirect_uri = '  '

const oAuth2Client: OAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uri)


export const getAuthUri = async (): Promise<string> => {
    return oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
}

export const getGoogleUserFromCode = async (code: string): Promise<any> => {
    return oAuth2Client.getToken(code).then(res => {
        const { id_token } = res.tokens
        const { given_name, family_name, email, locale } = AuthService.decodeJwtToken(id_token)        
        return { firstName: given_name, lastName: family_name, email: email }
    })
    // return oAuth2Client.getToken(code).then(res => {
    //     const { id_token } = res.tokens
    //     const { given_name, family_name, email, locale } = AuthService.decodeJwtToken(id_token)
    //     console.log(AuthService.decodeJwtToken(id_token));
        
    //     return { firstName: given_name, lastName: family_name, email: email }
    // })
}

