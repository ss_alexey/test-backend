import { UploadFile } from '../interfaces'
import {  IMAGE_FOLDER_ROUTE, DEFAULT_IMAGE_FORMAT, FILE } from '../config'
import { AuthService, createOriginalUserImageFileName, createImageFilePath, writeImageAvatar, createUserImageUrl, createAvatarUserImageFileName, writeImage } from '../helpers';
// import sharp = require('sharp');

import * as fs from 'fs'

interface profileImageParams {
    original: null | string,
    avatar: null | string
}

const dir = IMAGE_FOLDER_ROUTE
!fs.existsSync(dir) && fs.mkdirSync(dir);
export const saveUserProfileImage = async (image: UploadFile, token: string): Promise<profileImageParams> => {
    const { id } = await AuthService.tokenDecode(token)
    let urlParams: profileImageParams = {
        original: null,
        avatar: null
    }
    try {
        await createAvatarUserImageFileName(id).then(name => {
            createImageFilePath(name).then(path => {
                writeImageAvatar(image.buffer, path).then(() => {
                    createUserImageUrl(name).then(res => {
                        console.log('HERE AVATAR', res);
                        urlParams.avatar = res
                    })
                })
            })
        })
        await createOriginalUserImageFileName(id).then(name => {
            createImageFilePath(name).then(path => {
                writeImage(image.buffer, path).then(() => {
                    createUserImageUrl(name).then(res => {
                        urlParams.original = res
                        // urlParams.avatar = res
                    })
                })
            })
        })
    } catch (error) {
        throw error
    }

    return urlParams
}