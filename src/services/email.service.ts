import * as Email from 'nodemailer'

export const sendEmail = (email: string, sub: string, text: string) => {
    const transporter = Email.createTransport({
        service: 'gmail',
        auth: {
            user: 'youremail@gmail.com',
            pass: 'yourpassword'
        }
    });
    const mailOptions = {
        from: 'youremail@gmail.com',
        to: `${email}`,
        subject: `${sub}`,
        text: `${text}`
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

}