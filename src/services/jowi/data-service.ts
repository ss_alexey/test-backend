import { getRestaurants, getRestaurantById, getRestTables } from '../../helpers'
import { JoxiMenu, EstablishmentRegister, JowiEstablishment, DishRegister } from '@interfaces';

import { saveEstablishment, saveDishCategoryToDb, saveDishToDb } from '../../db/db-services'

export const getData = async () => {
    try {
        const { restaurants } = (await getRestaurants())
        restaurants.forEach((rest: JowiEstablishment) => {
            const newRest: EstablishmentRegister = {
                title: rest.title,
                phone: null,
                coordinate: {
                    latitude: rest.latitude,
                    longitude: rest.longitude
                },
                adress: {
                    city: rest.city,
                    country: rest.country,
                    street: rest.address,
                    number: 0
                },
                type: rest.restaurant_type,
                details: rest.description
            }
            saveEstablishment(newRest)
        });
        return restaurants
    } catch (error) {
        throw error
    }
}
export const getRestById = async (id?: string) => {
    if (!id) {
        id = 'a40b4cd6-222c-4f38-a010-83e5bf9c0da1'
    }
    try {
        const response = await getRestaurantById(id)
        const { categories } = response
        await categories.forEach(async cat => {
            const catId = (await saveDishCategoryToDb({ title: cat.title })).id
            await cat.courses.forEach(async course => {
                const newDish: DishRegister = {
                    price: course.price,
                    category: catId,
                    name: course.title,
                    img: course.image_url,
                    description: course.description
                }
                saveDishToDb(newDish)
            })
        })
        return response
    } catch (error) {
        throw error
    }
}
export const getTables = async (id?: string) => {
    if (!id) {
        id = 'a40b4cd6-222c-4f38-a010-83e5bf9c0da1'
    }
    return (await getRestTables(id))
}