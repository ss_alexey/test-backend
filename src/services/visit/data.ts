import { VisitRegister } from "@interfaces";
import { validateVisitRegister } from "../../validators";
import {AuthService} from '../../helpers'
import { saveVisit } from '../../db/db-services'

export const registerVisit = async (visit: VisitRegister, token: string) => {
    if(!visit.userId) {
        visit.userId = await AuthService.getIdFromToken(token)
    }
    await validateVisitRegister(visit)
    const resut = await saveVisit(visit)
    return resut.toJSON()
}