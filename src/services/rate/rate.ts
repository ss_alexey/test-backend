import { SaveRate } from '@interfaces'
import { validateRateRegister } from '../../validators';
import { saveRateDb, findUserById, getEstablishmentById } from '../../db/db-services'
import { AuthService } from '../../helpers';
import * as Boom from 'boom'
export const saveRate = async (rate: SaveRate, token: string) => {
    try {
        const userId = await AuthService.getIdFromToken(token)
        rate.owner = userId
        // if (!await findUserById(userId) || !await getEstablishmentById(rate.parent)) {
        //     throw Boom.notFound('no such data')
        // }
        await validateRateRegister(rate)
        return (await saveRateDb(rate))
        // return rate
    } catch (error) {
        throw error
    }
}