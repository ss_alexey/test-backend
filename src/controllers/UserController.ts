import { JsonController, Param, Body, HttpCode, Get, Post, Put, Delete, HttpError, HeaderParam, Authorized , } from 'routing-controllers';

import { getUserByToken } from '../services'

import {User} from '../db/models'

@JsonController('/user')

export class UserController {
    // @Authorized()
    // @Get('/self')
    // @HttpCode(200)
    // private async getSelf(@HeaderParam("authorization") token: string) {
    //     return (await getUserByToken(token))
    // }

    // // @Authorized()
    // @Get('/userById/:id')
    // private async getUser(@Param('id') id: string) {
    //     return (await getById(id))
    // }
    @Authorized()
    @Get('/self')
    @HttpCode(200)
    private async getSelf(@HeaderParam("authorization") token: string) {
        try {
            return {data: (await getUserByToken(token))}
        } catch (error) {
            throw error
        }
       
    }
    @Delete('/delete')
    async deleteUser(@Body() user: {email: string}) {
        try {
            await User.findOneAndDelete({email: user.email})
            return 'DELETED'
        } catch (error) {
            throw error
        }
    }

    // @Get('/users')
    // async getAll() {
    //     const users = (await User.find()).map(user => user.toJSON());
    //     return users;
    // }
    // @Get('/users/:id')
    // async getOne(@Param('id') id: string) {
    //     const result = await User.findById(id);
    //     const user = result && result.toJSON();
    //     if (!user) {
    //         throw new HttpError(404, `User was not found.`);
    //     }
    //     return user;
    // }
    // @Post('/users')
    // @HttpCode(201)
    // async post(@Body() { password, ...user }: UserPost) {
    //     if ((await User.find({ email: user.email })).length) {
    //         throw Boom.badRequest('User already exist');
    //     }
    //     new User({
    //         hash: authService.getHash(password),
    //         ...user
    //     }).save();
    //     return `${user.firstName} created`;
    // }

    // @Put('/users/:id')
    // async put(@Param('id') id: string, @Body() user: IUser) {
    //     await User.findByIdAndUpdate(id, user);
    //     return 'user edited';
    // }

    // @Delete('/users/:id')
    // async remove(@Param('id') id: string) {
    //     await User.findByIdAndRemove(id);
    //     return 'user deleted';
    // }
}
