import { JsonController, Post, Body, Get, HeaderParam, Authorized, HttpError, Params, HeaderParams, Param, QueryParams } from 'routing-controllers';
import { generatePin, makeTempUser, checkUserPin, AuthService } from '../helpers'
import { VisitRegister } from '@interfaces';
import { registerVisit } from '../services'
import { EstablishmentPlacesModel, EstablishmentTableModel, Establishment, EstablishmentReserveModel, User, Dish, EstablishmentOrdersModel } from '../db/models'

import { randomNumber } from '../fakeData/randomNumbers'

import * as Boom from 'boom'
import * as http from 'http'

import { parseTimeStamp } from '../helpers'

interface EstablishmentTable {
    placeModelId: number
    establishmentModelId: string
    place_count: number
    // table_id: number
}

interface EstablishmentsPlace {
    title: string,
    openair: boolean,
    establishmentId: string
    // modelId: string
    // place_id: number,
    // place_tables: EstablishmentTable[]
}
interface Preorder {
    dishId: string,
    dishPrice: number,
    dishCount: number
}
interface ResurveTable {

    from: number,
    people_count: number,
    establishment: string,
    tableId: string,
    placeId: string,
    preoreder?: Preorder[]
}

interface ReserveQuery {
    establishment: string,
    people_count?: number,
    date?: number
}

@JsonController('/test')
export class AuthController {
    @Post('/place')
    async registerPlace(@Body() place: EstablishmentsPlace) {
        try {
            if (!await Establishment.findById(place.establishmentId)) {
                throw 'error'
            }
            const placeModelId = (await new EstablishmentPlacesModel(place).save()).id
            await Establishment.findByIdAndUpdate(place.establishmentId, { $push: { places: placeModelId } })
            return 'HI'
        } catch (error) {
            throw error
        }


    }
    @Post('/table')
    async registerTable(@Body() table: EstablishmentTable) {
        try {
            if (!(await Establishment.findById(table.establishmentModelId)) || !(await EstablishmentPlacesModel.findById(table.placeModelId))) {
                throw 'error'
            }
            const tableId = (await new EstablishmentTableModel(table).save()).id
            await EstablishmentPlacesModel.findByIdAndUpdate(table.placeModelId, { $push: { tables: tableId } })
            return 'HI'
        } catch (error) {
            throw error
        }

    }
    @Authorized()
    @Post('/reserve')
    async reserveTable(@Body() reserve: ResurveTable, @HeaderParam("authorization") token: string, ) {
        try {
            const userId = await AuthService.getIdFromToken(token)
            const user = (await User.findById(userId))
            if (!user) {
                throw Boom.notFound('no such user')
            }
            if (!user.phone) {
                if (user.email === 'pavlov.sergey.alexandr@gmail.com') {
                    await User.findByIdAndUpdate(userId, { $set: { phone: "+30505555555" } })
                    throw Boom.notAcceptable('phone is reqired. Чтобы не ебаться, добаляю тебе телефон "+30505555555"')
                }
                throw Boom.notAcceptable('phone is reqired.')
            }


            const table = (await EstablishmentTableModel.findById(reserve.tableId))
            const est = (await Establishment.findById(reserve.establishment)).toJSON()
            const place = (await EstablishmentPlacesModel.findById(reserve.placeId))
            // const otherReserve = (await EstablishmentReserveModel.find({ establishment: reserve.establishment, tableId: reserve.tableId, placeId: reserve.placeId })).map(res => res.toJSON())
            if (!table || !est || !place || table.place_count < reserve.people_count) {
                throw 'errror'
            }
            const newReserve = (await new EstablishmentReserveModel(reserve).save())


            if (reserve.preoreder) {
                const preorder = reserve.preoreder
                await Promise.all(preorder.map(async (order) => {
                    const res = await Dish.findById(order.dishId)
                    if (!res) {
                        throw Boom.notFound('no such dish')
                    }
                    return res
                }))
                const total = reserve.preoreder.reduce((result: number, next: Preorder) => {
                    return result + next.dishCount * next.dishPrice
                }, 0)
                const testPre = await new EstablishmentOrdersModel({
                    owner: userId, reserveId: newReserve._id, total: total, orders: preorder.map(i => {
                        const t = { dish: i.dishId, count: i.dishCount }
                        return t

                    })
                }).save()
                await EstablishmentReserveModel.findByIdAndUpdate(newReserve._id, { $set: { preorderModelId: testPre._id } })
            }
            const esta = (await Establishment.findById(reserve.establishment)).toJSON()
            const fl = reserve.preoreder.map((or: any) => {
                return { ...or, count: or.dishCount }

            })
            const test = (await EstablishmentReserveModel.findById(newReserve._id).populate({ path: 'preorderModelId', populate: {path: 'orders.dish'} }).populate('establishment')).toJSON()
            // console.log(test);

            // return esta
            reserve = { ...reserve, establishment: esta, preoreder: fl, id: newReserve._id.toJSON() } as any
            return test
        } catch (error) {
            console.log(error);

            throw error
        }
    }
    @Get('/reserve')
    async getvalidReserve(@QueryParams() params: ReserveQuery) {
        const date = (params.date ? params.date : Date.now())
        // const date = Date.now()
        const establish = (await Establishment.findById(params.establishment).populate('information').populate({ path: 'places', populate: { path: 'tables' } })).toJSON()
        const places = establish.places
        const hasFree = async () => {
            let ordered = 0
            let free = 0
            const reserves = (await EstablishmentReserveModel.find({ establishment: params.establishment })).map(res => res.toJSON())
            places.forEach((place: any) => {
                place.tables.forEach((table: any) => {
                    if (reserves.find((reserve) => reserve.tableId === table.id)) {
                        ordered++
                    } else {
                        free++
                    }
                })
            })
        }
        await hasFree()
        const workTime = establish.information.workTime

        const { year, month, day, weekday, hour, min, sec } = parseTimeStamp(date)
        // console.log(workTime);
        const isDayOff = workTime[weekday].day_off
        // console.log(isDayOff);

        const getNextWeekDay = (date: number): boolean => {
            return workTime[parseTimeStamp(date).weekday].day_off
        }

        const getClosestWorkDay = (date: number) => {
            let newDate = date + 1 * 3600 * 24 * 1000

            for (let i = newDate; getNextWeekDay(i) === false; i += 1 * 3600 * 24 * 1000) {
                if (i > date + 1 * 3600 * 24 * 1000 * 7) {
                    newDate = null
                    break
                }
                newDate = i
            }

            return newDate
        }
        // const estTables = 
        const test = getClosestWorkDay(date)
        const available: {
            today: boolean,
            closest?: any
        } = {
            today: !isDayOff
        }
        if (!isDayOff) {
            available.closest = getClosestWorkDay(date)
        }


        const result = (await EstablishmentReserveModel.find({ establishment: params.establishment })).map(res => {
            res = res.toJSON()
            res.from = res.from / 1000
            return res
        })


        return available

    }

}




