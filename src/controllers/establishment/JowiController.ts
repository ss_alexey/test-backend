import { JsonController, Post, Body, ContentType, HttpCode, QueryParams, UploadedFile, HeaderParams, HttpError, Get, Authorized, Header, HeaderParam, Put, Param } from 'routing-controllers';
import { LoginUser, RegisterUser, JoxiMenu } from '../../interfaces'
import { AuthService } from '../../helpers'
import {  getData, getRestById } from '../../services'



@JsonController('/jowi')
export class JowiController {
    @Get('/')
    async get() {
        return (await getData())
    }
    @Get('/:id')
    async getById(@Param('id') id: string): Promise<JoxiMenu> {
        return (await getRestById(id))
    }
}




