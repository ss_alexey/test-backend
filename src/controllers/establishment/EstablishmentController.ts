import {
  JsonController,
  Post,
  Body,
  Get,
  UploadedFile,
  Param,
  HeaderParam,
  Put,
  QueryParams
} from 'routing-controllers';
import {
  registerEstablishment,
  getAllEstablishments,
  getEstablishmentById,
  updateEstablishmentParams
} from '../../services';
import { EstablishmentRegister, UploadFile, EstablishmentDetails } from '../../interfaces';

import { Rate } from '../../db/models'



@JsonController('/establishment')
export class EstablishmentController {
  @Post('/')
  async saveEstablishment(
    @UploadedFile('file') file: UploadFile,
    @Body() { body }: any
  ) {
    try {
      const est: EstablishmentRegister = JSON.parse(body);
      return await registerEstablishment(est, file);
    } catch (error) {
      throw error
    }


  }

  @Get('/')
  async getEstablishments(@HeaderParam("authorization") token: string) {

    try {
      console.time('establishment querry')
      const result = { data: (await getAllEstablishments(token, 5)) };


      console.timeEnd('establishment querry')


      return result
    } catch (error) {
      throw error
    }
  }
  @Get('/:id')
  async getEstablishmentById(@Param('id') id: string, @QueryParams() params: any) {
    try {
      if (params) {
        console.log(params);

      } else {
        console.log('NO QUERY');

      }
      const rest = (await getEstablishmentById(id))
      // const similar = (await getAllEstablishments())
      //   .filter(res => {
      //     if (res.type === rest.type && res.id !== rest.id) {
      //       return true
      //     }
      //     return false
      //   })
      //   .slice(0, 5)

      // return { data: { ...rest, similar: similar } }
      return {data: rest}
      // return { data: await getEstablishmentById(id) };
    } catch (error) {
      throw error;
    }
  }
  // @Put('/information/:id/addException')
  // async updateInformation(@Param('id') id: string, @Body()date){

  // }
  @Put('/details/:id')
  async updateEstablishment(@Param('id') id: string, @Body() est: EstablishmentDetails) {
    try {
      return (await updateEstablishmentParams(est, id))
    } catch (error) {
      throw error
    }
  }
  @Get('/rates/:id')
  async getRates(@Param('id') id: string, ) {
    try {
      return (await Rate.findById(id)).toJSON()
    } catch (error) {
      throw error
    }
  }
}
