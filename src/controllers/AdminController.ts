import {
  JsonController,
  Post,
  Body,
  HttpCode,
  QueryParams,
  Get,
  Authorized,
  Params,
  UploadedFile,
  Header
} from 'routing-controllers';
// import { LoginUser, RegisterUser } from '../interfaces'
import { RegisterUser, LoginUser, RegisterAdminUser } from '@interfaces';

import {  loginUser, userRegister } from '../services';
import { json } from 'body-parser';

// import { testValidate } from '../validators';

@JsonController('/admin/auth')
export class AuthController {
  @Post('/login')
  @HttpCode(200)
  private async login(@Body() user: LoginUser): Promise<any> {
    try {

      return { data: 'await userRegister(user)' };
    } catch (e) {
      throw e;
    }
  }
  @Post('/register')

  private async register(
    @UploadedFile('file') file: any,
    @Body() user: RegisterUser
  ) {
    try {
      return (await userRegister(user))
    } catch (error) {
      throw error;
    }
  }
  // @Authorized()
  // @Get('/testToken')
  // private async testToken() {
  //     return "SUCCESS"
  // }
}
