import { JsonController, Param, Body, HttpCode, Get, Post, Put, Delete, HttpError, HeaderParam, Authorized } from 'routing-controllers';
import { testTime, NewDate } from '../helpers/time/test-time'
// import { getUserByToken, getById } from '../sevices'
import { EstablishmentDetailsModel } from '../db/models'
import { getRestaurants } from '../helpers'

interface JoinPosterQuery {
    account: string,
    object: 'catergory',
    object_id: number,
    action: 'added' | 'changed' | 'removed' | 'transformed'
    time: string,
    verify: string,
    account_number: number
}
type JoinPosterApp = {

    account: string,
    object: 'application',
    object_id: number,
    action: 'added' | 'changed' | 'removed' | 'transformed'
    time: string,
    verify: string,
    account_number: number,
    data: {
        user_id: number,
        ownerInfo: {
            email: string,
            phone: string,
            country: string,
            name: string,
            company_name: string
        },
        tariff: any[]
    }
}

@JsonController('/main')




export class AUserController {
    @Post('/joinposter')
    @HttpCode(200)
    private async joinposter(@Body() body: any) {
        console.log(body);
        return {
            status: 'accept'
        }
    }
}

const registerEst = () => {

}
