import {
  JsonController,
  Post,
  Body,
  HttpCode,
  QueryParams,
  Get,
  Authorized,
  Params,
  UploadedFile
} from 'routing-controllers';
import { RegisterUser, LoginUser } from '@interfaces';

import { loginUser, userRegister, confirmPhone, generateNewPin ,temporaryGoogleLogin ,loginFaceBookUser} from '../services';

import {AuthService} from '../helpers'
import {User} from '../db/models'

import * as Boom from 'boom'

@JsonController('/auth')
export class AuthController {
  @Post('/getPin')
  async getPin(@Body() params: { phone: string }) {
    try {
      return (await generateNewPin(params.phone))
    } catch (error) {
      console.log(error); 
      throw error
    }
  }
  @Post('/varifyPhone')
  async varifyPhone(@Body() params: { phone: string, pin: string }): Promise<{ token: string }> {
    try {
      if (!params.phone || !params.pin) {
        throw Boom.notAcceptable('phone and pin needed')
      }
      return (await confirmPhone(params.phone, params.pin))
    } catch (error) {
      console.log(error);
      throw error
    }

  }
  // @Get("/test")
  // private async testAuth() {
  //     return (await getAuthUri())
  // }
  @Post('/faceBookLogin')
  private async postFaceBookLogin(@Body() user: { email: string, name: string, url: string }) {
      return { token: (await loginFaceBookUser(user)) }

  }
  @Post('/googleLogin')
   async postGoogleLogin(@Body() body: { idToken: string }) {
      return { token: (await temporaryGoogleLogin(body.idToken)) }
  }

  // @Get('/google')
  // async asd() {
  //     console.log('GET');
  //     return 'HI'
  // }
  // // @Post(/)
  // @Get('/login')
  // async as(@QueryParams() params: any) {
  //     console.log(params);

  //     return params
  // }
  @Post('/login')
  @HttpCode(200)
  private async login(@Body() user: LoginUser): Promise<any> {
    try {
      const result = await loginUser(user);
      return { data: result };
    } catch (e) {
      throw e;
    }
  }
  @Post('/register')
  private async register(@UploadedFile('file') file: any, @Body() user: any): Promise<any> {
    try {
      return { data: await userRegister(user as RegisterUser, ['apiUser', 'register']) };
    } catch (error) {
      throw error;
    }
  }
}
