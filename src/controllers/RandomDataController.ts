import { JsonController, Param, Body, HttpCode, Get, Post, Put, Delete, HttpError, HeaderParam, Authorized } from 'routing-controllers';
import { saveFakeData } from '../fakeData/saveFakeData'

interface RandomData {
    userCount: number
    commentCount: number
    likeCount: number
    rateCount: number
    visitCount: number
    userPhotoCount: number
    establishmentPhotoCount: number
    assignUserAvatars: boolean
    rating: {
        max: number,
        min: number
    },
    generateTables: boolean

}
@JsonController('/random')



export class UserController {

    @Post('/')
    @HttpCode(200)
    private async randomData(@Body() data: RandomData) {

        try {

            await saveFakeData(data)
            return { success: true }
        } catch (error) {
            console.log(11111, error);

            throw error
        }


    }
}
