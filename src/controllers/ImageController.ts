import { JsonController, Post, Body, ContentType, HttpCode, QueryParams, UploadedFile, HeaderParams, HttpError, Get, Authorized, Header, HeaderParam, Put, Param } from 'routing-controllers';
import { LoginUser, RegisterUser } from '../interfaces'
import { AuthService } from '../helpers'
// import { saveUserProfileImage } from '../sevices'
import * as sharp from 'sharp'
import { IMAGE_FOLDER_ROUTE, DEFAULT_IMAGE_FORMAT } from '../config'
import { File } from '../db/connections'




@JsonController('/image')
export class AuthController {
    @Get('/:id')
    @ContentType('image/jpg')
    async getImage(@Param('id') id: string) {
        try {
            return (await File.findById(id)).file
        } catch (error) {
            throw error
        }

    }
    // @Authorized()
    // @Post('/')
    // async saveFile(@UploadedFile('image') file: Image, @Body() body: any, @HeaderParam("authorization") token: string) {
    //     try {
    //         await saveUserProfileImage(file, token)
    //         return { data: true }
    //     } catch (error) {
    //         throw error
    //     }


    // }
}

