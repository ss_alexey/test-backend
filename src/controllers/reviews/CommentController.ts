import { JsonController, Post, Body, Get, HeaderParam, Authorized } from 'routing-controllers';
import { RegisterComment } from '@interfaces'
import { registerComment } from '../../services'

@JsonController('/comment')
export class CommentController {

    @Authorized(['user'])
    @Post('/')
    async addcomment(@HeaderParam("authorization") token: string, @Body() comment: RegisterComment) {
        try {
            const result = await registerComment(comment, token)
            return result
        } catch (error) {
            throw error
        }
    }
    @Get('/')
    async getComments() {
        return 'res'
    }
}




