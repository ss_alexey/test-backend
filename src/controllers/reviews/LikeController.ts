import {
    JsonController,
    Post,
    Body,
    HttpCode,
    QueryParams,
    Get,
    Authorized,
    Params,
    UploadedFile,
    HeaderParam
} from 'routing-controllers';
// import { LoginUser, RegisterUser } from '../interfaces'
import { RegisterUser, LoginUser, SaveLike } from '@interfaces';

import { saveLike } from '../../services';

@JsonController('/like')
export class AuthController {
    @Authorized()
    @Post('/')
    async saveNewLike(@HeaderParam("authorization") token: string, @Body() like: SaveLike) {
        try {
            const res = await saveLike(like, token)
            return { data: res.toJSON() }
        } catch (error) {
            throw error
        }
    }
}
