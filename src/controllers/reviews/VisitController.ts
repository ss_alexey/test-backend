import { JsonController, Post, Body, Get, HeaderParam, Authorized, HttpError } from 'routing-controllers';
import { generatePin, makeTempUser, checkUserPin } from '../../helpers'
import { VisitRegister } from '@interfaces';
import { registerVisit } from '../../services'
import { docs } from '../../db/models'
// import fetch from 'fe'
import * as Boom from 'boom'
import * as http from 'http'
@JsonController('/visit')
export class AuthController {

    @Authorized()
    @Post('/')
    async newVisit(@HeaderParam("authorization") token: string, @Body() visit: VisitRegister) {
        try {
            return (await registerVisit(visit, token))
        } catch (error) {
            throw error
        }
    }

}




