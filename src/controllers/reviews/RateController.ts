import {
    JsonController,
    Post,
    Body,
    HeaderParam,
    Authorized
} from 'routing-controllers';
import { SaveRate } from '@interfaces';

import { saveRate } from '../../services';

@JsonController('/rate')
export class AuthController {
    @Authorized()
    @Post('/')
    async saveNewRate(@HeaderParam("authorization") token: string, @Body() rate: SaveRate) {
        try {
            return (await saveRate(rate, token))
        } catch (error) {
            throw error
        }
    }
}
