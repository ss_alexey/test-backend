export const collNames = {
    User: 'User',
    Establishment: 'Establishment',
    Category: 'Category',
    Dish: 'Dish',
    File: 'File'
}
