
import { collNames } from './collectionNames'
import * as schemas from '../schemas'
import * as Model from '../models'
import { connection } from '../../server'

// export const DishCategory = connection.model<Model.DishCategoryModel>(collNames.Category, schemas.dishCategorySchema)
// export const Dish = connection.model<Model.DishModel>(collNames.Dish, schemas.dishSchema)
export const File = connection.model<any>(collNames.File, schemas.fileSchema)
