import { collNames } from '../../db/connections'
import { Schema, models } from 'mongoose';
import * as mongoose from 'mongoose'
import { RegisterUser, EstablishmentParams } from '../../interfaces';


// import { DishModel } from '../models';
import { AuthService } from '../../helpers'



export const fileSchema: Schema<any> = new Schema<any>(
    {
        file: Buffer,
    },
    {
        toJSON: {
            transform: (doc, ret) => {
                ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);

// fileSchema.pre<DishModel>('save', function (next, done: any) {

//     next();
// });
