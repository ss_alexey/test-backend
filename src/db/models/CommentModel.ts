import { Document, Schema } from 'mongoose';
import { CommentParams } from '../../interfaces'
import { connection } from '../../server'

import { Establishment } from './EstablishmentModel'

import { randomNumber } from '../../fakeData/randomNumbers'

type Model = Document & CommentParams & {
    createdAt: Date
}
const schema: Schema<Model> = new Schema<Model>(
    {
        voted: Boolean,
        text: String,
        owner: { type: Schema.Types.ObjectId, ref: 'user' },
        parent: { type: Schema.Types.ObjectId, refPath: 'establishment' },
        createdAt: Date
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {

                ret.voted = () => {
                    const vote = randomNumber(2, 0)
                    return {
                        activeUseful: vote === 2 ? true : false,
                        activeNotUseful: vote === 1 ? true : false
                    }
                }
                ret.parent = ret.parent.toJSON();
                ret.id = ret._id.toJSON();
                if (ret.like) {
                    ret.like = ret.like.length || 0
                }
                if (ret.dislike) {
                    ret.dislike = ret.dislike.length || 0
                }

                delete ret._id
                delete ret.__v;
                delete ret.password;
            }
        }
    }
);

schema.pre<Model>('save', async function (next, done: any) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    try {
        await Establishment.findByIdAndUpdate(this.parent, { $push: { comments: this._id } })
    } catch (error) {
        throw error
    }
    next();
});
schema.virtual('like', {
    ref: 'like',
    localField: '_id',
    foreignField: 'parent',
    // count: true,
    // justOne: false
});
schema.virtual('dislike', {
    ref: 'like',
    localField: '_id',
    foreignField: 'parent',
    // count: true,
    // justOne: false
});

export const Comment = connection.model<Model>('comment', schema);
export const docs = async () => {
    // await Comment.updateMany({ text: { $exists: true } }, (err: any, res: any) => {
    //     console.log(res);
    // })
    // return (await Comment.find({ text: { $exists: true } })).map(res => res.toJSON())
    return
};