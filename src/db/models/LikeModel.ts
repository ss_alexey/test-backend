import { Document, Schema } from 'mongoose';
import { SaveLike } from '../../interfaces';
import { connection } from '../../server';

type Model = Document &
    SaveLike & {
        createdAt: Date;
    };

const schema: Schema<Model> = new Schema<Model>(
    {
        value: Boolean,
        owner: { type: Schema.Types.ObjectId, ref: 'user' },
        parent: { type: Schema.Types.ObjectId, ref: 'comment' }
    },
    {
        toJSON: {
            transform: (doc, ret) => {
                ret.id = ret._id.toJSON();
                ret.owner= ret.owner.toJSON()
                ret.parent = ret.parent.toJSON()
                delete ret._id;
                delete ret.__v;
            }
        }
    }
);
schema.pre<Model>('save', function (next, done: any) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

export const Like = connection.model<Model>('like', schema);
