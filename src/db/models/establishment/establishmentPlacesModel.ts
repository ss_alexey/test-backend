import { Document, Schema } from 'mongoose';
// import { CommentParams } from '../../interfaces'
import { connection } from '../../../server'

// import { Establishment } from './EstablishmentModel'

// import { randomNumber } from '../../fakeData/randomNumbers'

interface EstablishmentTable {
    place_count: number
    table_id: number
}

interface EstablishmentsPlace {
    title: string,
    openair: boolean
    place_id: number,
    place_tables: EstablishmentTable[]
}





type Model = Document
const schema: Schema<Model> = new Schema<Model>(
    {
        establishmentModelId: { type: Schema.Types.ObjectId, ref: 'establishment' },
        tables: [{ type: Schema.Types.ObjectId, ref: 'establishmentTable' }],
        title: String,
        openAir: Boolean
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {



                delete ret._id
                delete ret.__v;
                delete ret.password;
            }
        }
    }
);

schema.pre<Model>('save', async function (next, done: any) {

    next();
});



export const EstablishmentPlacesModel = connection.model<Model>('establishmentPlaces', schema);