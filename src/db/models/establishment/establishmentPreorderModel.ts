import { Document, Schema } from 'mongoose';
// import { CommentParams } from '../../interfaces'
import { connection } from '../../../server'

// import { Establishment } from './EstablishmentModel'

// import { randomNumber } from '../../fakeData/randomNumbers'

// interface EstablishmentTable {
//     place_count: number
//     table_id: number
// }

// interface EstablishmentsPlace {
//     title: string,
//     openair: boolean
//     place_id: number,
//     place_tables: EstablishmentTable[]
// }





type Model = Document
const schema: Schema<Model> = new Schema<Model>(
    {
        owner: { type: Schema.Types.ObjectId, red: 'user' },
        reserveId: { type: Schema.Types.ObjectId, ref: 'establishmentReserve' },
        orders: [{ dish : {type: Schema.Types.ObjectId, ref: 'dish'}, count: Number }],
        total: Number
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {
                delete ret._id
            }
        }
    }
);

schema.pre<Model>('save', async function (next, done: any) {

    next();
});



export const EstablishmentOrdersModel = connection.model<Model>('establishmentOrder', schema);