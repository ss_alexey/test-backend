import { Document, Schema } from 'mongoose';
// import { CommentParams } from '../../interfaces'
import { connection } from '../../../server'

// import { Establishment } from './EstablishmentModel'

// import { randomNumber } from '../../fakeData/randomNumbers'






type Model = Document & {
    from: number,
    preorderModelId? : string
}
const schema: Schema<Model> = new Schema<Model>(
    {
        reserveOwner: { type: Schema.Types.ObjectId, ref: 'user' },
        from: Number,
        people_count: Number,
        establishment: { type: Schema.Types.ObjectId, ref: 'establishment' },
        tableId: { type: Schema.Types.ObjectId, ref: 'establishmentTable' },
        placeId: { type: Schema.Types.ObjectId, ref: 'establishmentPlaces' },
        preorderModelId: {type: Schema.Types.ObjectId, ref: 'establishmentOrder'}
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {
                // ret.reserveOwner = ret.reserveOwner.toJSON()
                // ret.tableId = ret.tableId.toJSON()
                // ret.establishment = ret.establishment.toJSON()
                // ret.placeId = ret.placeId.toJSON()
                delete ret._id
                delete ret.__v;
                delete ret.password;
            }
        }
    }
);

schema.pre<Model>('save', async function (next, done: any) {

    next();
});



export const EstablishmentReserveModel = connection.model<Model>('establishmentReserve', schema);