import { Document, Schema } from 'mongoose';
import { connection } from '../../../server'


interface EstablishmentTable {
    place_count: number
    table_id: number
}

interface EstablishmentsPlace {
    title: string,
    openair: boolean
    place_id: number,
    place_tables: EstablishmentTable[]
}





type Model = Document & EstablishmentTable
const schema: Schema<Model> = new Schema<Model>(
    {
        place_count: Number,
        placeModelId: Schema.Types.ObjectId,
        establishmentModelId: Schema.Types.ObjectId
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {
                ret.establishmentModelId = ret.establishmentModelId.toJSON()
                ret.placeModelId = ret.placeModelId.toJSON()
                delete ret._id
                delete ret.__v;
                delete ret.password;
            }
        }
    }
);

schema.pre<Model>('save', async function (next, done: any) {

    next();
});



export const EstablishmentTableModel = connection.model<Model>('establishmentTable', schema);