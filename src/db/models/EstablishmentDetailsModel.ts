import { Document, Schema } from 'mongoose';

import { EstablishmentParams, EstablishmentDetails } from '../../interfaces'
import { connection } from '../../server'

import { Category } from '../models'


// const images: any[] = ['http://static.asiawebdirect.com/m/bangkok/portals/bali-indonesia-com/homepage/top10/top10-restaurants-sanur/pagePropertiesImage/best-restaurants-sanur.jpg.jpg',
//     'http://www.cateringvalle.com/wp-content/uploads/2018/08/Co-Restaurants.jpg',
//     'https://www.pinstrikes9.com/resourcefiles/mainimages/pin-strikes-georgia-restaurant-top.jpg'
// ]

type Model = Document & EstablishmentDetails
const schema: Schema<Model> = new Schema<Model>(
    {
        userPhotos: [{ owner: { type: Schema.Types.ObjectId, ref: 'user' }, imgUrl: String , _id: false}],
        photos: [String],
        description: String,
        menu: Schema.Types.Mixed,
        establishment: { type: Schema.Types.ObjectId, ref: 'establishment' }
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {
                if(ret.photos.length && ret.photos.length > 5) {
                    ret.photos as [string]
                    ret.photos = ret.photos.slice(0,5)
                }
                if (ret.menu.dishes) {
                    ret.menu = ret.menu.dishes.map((dish: any) => {
                        dish.id.toJSON()
                        return dish
                    })
                }
                ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);

schema.pre<Model>('save', async function (next, done: any) {
    if (!this.menu) {
        const result = (await Category.find().populate('dishes')).map(res => {
            const result = res.toJSON()
            result.dishes = result.dishes.map((dish: any) => {
                dish.active = false
                const { category, ...result } = dish
                return result
            })
            return result

        })
        this.menu = result
    }
    this.userPhotos = []
    this.photos = []
    // this.photos = images
    if (!this.description) {
        this.description = ''
    }

    next();
});
schema.virtual('currentUserVisit', {
    ref: 'visit',
    localField: '_id',
    foreignField: 'establishmentId',
    count: true,
    justOne: false
});
export const EstablishmentDetailsModel = connection.model<Model>('establishmentDetails', schema)