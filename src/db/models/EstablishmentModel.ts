import { Document, Schema, SchemaTypes } from 'mongoose';

import { EstablishmentParams, EstablishmentCoordinate } from '../../interfaces'
import { connection } from '../../server'

import { hasFreePlaces } from '../../helpers'

import { getRandomName, getRandomSite, getRandomType, getRandomImage } from '../../fakeData/static'

const getRan = (min: number, max: number) => {
    return +(Math.random() * (max - min) + min).toFixed(6);
}

const getRandomCoordinates = (): EstablishmentCoordinate => {
    return { latitude: getRan(47.784129, 47.913603), longitude: getRan(35.087995, 35.297404) }
}

type Model = Document & EstablishmentParams & {
    coordinate: {
        latitude: number,
        longitude: number
    },
    location: {
        type: string,
        coordinates: [number, number]
    },
    average: number,
    rating: string,
    details: String,
    createdAt: Date,
    comments: string[]
}
const schema: Schema<Model> = new Schema<Model>(
    {
        site: String,
        coordinate: {
            latitude: Number,
            longitude: Number
        },
        location: {
            type: { type: String, default: "Point" },
            coordinates: []
        },
        title: String,
        type: String,
        avagarePrice: Number,
        adress: {
            number: Number,
            street: String,
            city: String,
            country: String,
        },
        places: [{ type: SchemaTypes.ObjectId, ref: 'establishmentPlaces' }],
        information: { type: Schema.Types.ObjectId, ref: 'establishmentInformation' },
        details: { type: Schema.Types.ObjectId, ref: 'establishmentDetails' },
        rating: {
            average: Number,
            modelId: { type: Schema.Types.ObjectId, ref: 'rate' },
            voteCount: Number,
            lastVote: Number
        },
        phone: String,
        image: String,
        comments: [{ type: Schema.Types.ObjectId, ref: 'comment' }]
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {
                if (ret.freePlaces && ret.freePlaces.length > -1) {
                    ret.freePlaces = hasFreePlaces(ret.places, ret.freePlaces)
                }
                ret.site = getRandomSite()
                ret.title = getRandomName()
                ret.type = getRandomType()
                ret.image = getRandomImage()
                ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);
schema.index({ location: "2dsphere" });
schema.pre<Model>('save', async (next, done: any) => {
    next();
});

schema.virtual('freePlaces', {
    ref: 'establishmentReserve',
    localField: '_id',
    foreignField: 'establishment',
})

schema.virtual('currentUserVisit', {
    ref: 'visit',
    localField: '_id',
    foreignField: 'establishmentId',
    count: true,
    justOne: false
});
export const Establishment = connection.model<Model>('establishment', schema)
console.log('REMOVE THIS');


// {
//     near: { type: "Point", coordinates: 
//     [  
//       35.257658,
    
//       47.828353
//       ] },
//       distanceField: "dist",
//       maxDistance: 1000,
//       distanceMultiplier:0.001,
//       num: 1,
//       spherical: true
// }

// Establishment.find().exec().then(res => {
//     res.forEach(i => {
//         // if (!i.location) {
//             console.log('HERE');
//             console.log(i.id);
            
//             Establishment.findByIdAndUpdate(i.id, { $set: { location: { type: 'Point', coordinates: [i.coordinate.latitude, i.coordinate.longitude] } } }).exec()
//         // }
//     })

// })
