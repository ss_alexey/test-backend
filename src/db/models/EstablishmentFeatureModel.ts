import { Document, Schema } from 'mongoose';

import { EstablishmentFeature } from '../../interfaces'
import { connection } from '../../server'

import { getIfOpened } from '../../helpers'


type Model = Document & EstablishmentFeature
const schema: Schema<Model> = new Schema<Model>(
    {
        value: String,
        key: String
    },
    {
        toJSON: {
            // virtuals: true,
            transform: (doc, ret) => {
                // ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);

schema.pre<Model>('save', async (next, done: any) => {
    next();

});
const features: {
    [key: string]: string
} = {
    live_music: 'живая музыка',
    wi_fi_zone: 'зона wi-fi',
    smoking_zone: 'зона для курящих',
    non_smoking_zone: 'зона для некурящих',
    veggie_dishes: 'вегитарианская кухня',
    open_halls: 'летние площадки',
    room_for_children: 'детская комната',
    online_order: 'онлайн заказ',
    online_reserve: 'онлайн бронирование',
}

export const EstablishmentFeatureModel = connection.model<Model>('establishmentFeature', schema)
EstablishmentFeatureModel.find().exec().then(res => {
    if (!res.length) {
        for (let key in features) {
            new EstablishmentFeatureModel({ key: key, value: features[key] }).save()
        }
    
    }
})