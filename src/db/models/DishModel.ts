import { Document } from 'mongoose';
import { DishParams } from '../../interfaces'

import { connection } from '../../server'

export type Model = Document & DishParams & {
    img: string
}

import { Schema } from 'mongoose';

const schema: Schema<Model> = new Schema<Model>(
    {   
        category: { type: Schema.Types.ObjectId, ref: 'category' },
        name: String,
        price: Number,
        img: String,
        description: String
    },
    {
        toJSON: {
            transform: (doc, ret) => {
                ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);

schema.pre<Model>('save', function (next, done: any) {
    if (!this.img) {
        this.img = null
    }
    next();
});
export const Dish = connection.model<Model>('dish', schema)
