import { Document, Schema } from 'mongoose';
import { UserParams } from '../../interfaces';
import { connection } from '../../server';
import { Validator } from 'class-validator'
import { AuthService } from '../../helpers';

import * as Boom from 'boom'
const validator = new Validator
type Model = Document &
  UserParams & {
    createdAt: Date;
    lastVisit: Date;
  };

const schema: Schema<Model> = new Schema<Model>(
  {
    avatar: String,
    firstName: {
      type: String,
      // validate: (value: string) => {
      //   if (!validator.isAlpha(value)) {
      //     const message = 'first name must contain just latin'
      //     throw Boom.notAcceptable(message, { firstName: message })
      //   }
      //   return true
      // }
    },
    password: String,
    email: {
      type: String,
      validate: (value: string) => {
        if (!validator.isEmail(value)) {
          const message = 'invalid email'
          throw Boom.conflict(message, { email: message })
        }
        return true
      }
    },
    confirmPassword: String,
    phone: {
      type: String,
      validate: (value: string) => {
        if (!validator.isPhoneNumber(value, 'UA')) {
          const message = 'invalid phone number'
          throw Boom.notAcceptable(message, { phone: message })
        }
        return true
      }
    }
  },
  {
    toJSON: {
      virtuals: true,
      transform: (doc, ret) => {
        ret.id = ret._id.toJSON();
        delete ret._id;
        delete ret.__v;
        delete ret.password;
      }
    }
  }
);

schema.pre<Model>('save', function (next, done: any) {
  if (this.password) {
    if (!this.confirmPassword) {
      const message = 'confirm password empty'
      throw Boom.notAcceptable(message, { confirmPassword: message })
    }
    if (this.password !== this.confirmPassword) {
      const message = 'confirm password is wrong'
      throw Boom.notAcceptable(message, { confirmPassword: message })
    }
    this.confirmPassword = undefined
    this.password = AuthService.getHashed(this.password);
  }
  if (!this.lastVisit) {
    this.lastVisit = new Date();
  }
  if (!this.createdAt) {
    this.createdAt = new Date();
  }
  next();
}, (err) => {
  console.log('ERROR', err);

});
schema.virtual('visit', {
  ref: 'visit',
  localField: '_id',
  foreignField: 'userId',
  count: true,
  justOne: false
});

export const User = connection.model<Model>('user', schema);

User.on('index', function (err) {
  console.log('INDEXING USER COLLECTION');

  if (err) {
    console.log('INDEX ERROR', err);

    throw err
  }
}
)
