import { Document } from 'mongoose';
import { DishCategoryParams } from '../../interfaces'

import { connection } from '../../server'

import {getRestById} from '../../services'

type Model = Document & DishCategoryParams & {
    dishes: string[]
    createdAt: Date
}
import { Schema } from 'mongoose';

export const schema: Schema<Model> = new Schema<Model>(
    {
        title: String,
        dishes: [{ type: Schema.Types.ObjectId, ref: 'dish' }]
    },
    {
        toJSON: {
            transform: (doc, ret) => {
                ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);

schema.pre<Model>('save', function (next, done: any) {
    if (!this.dishes) {
        this.dishes = []
    }
    next();
});
export const Category = connection.model<Model>('category', schema)
Category.find().exec().then(res => {
    if (!res.length) {
        getRestById()
    }
})
