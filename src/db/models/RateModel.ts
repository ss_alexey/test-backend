import { Document, Schema } from 'mongoose';
import { SaveRate } from '@interfaces';
import { connection } from '../../server';

type Model = Document &
    SaveRate & {
        createdAt: Date;
    };


const addVoteFields = (count: number) => {
    const voteFields: {
        [key: string]: [{ type: any, ref: any }]
    } = {}
    for (let i = 1; i <= count; i++) {
        voteFields[i] = [{ type: Schema.Types.ObjectId, ref: 'user' }]
    }
    return voteFields
}

const schema: Schema<Model> = new Schema<Model>(
    {
        average: Number,
        target: { type: Schema.Types.ObjectId, ref: 'establishment' },
        votes: addVoteFields(10)
    },
    {
        toJSON: {
            transform: (doc, ret) => {
                ret.id = ret._id.toJSON();
                delete ret._id;
                delete ret.__v;
            }
        }
    }
);
schema.pre<Model>('save', function (next, done: any) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});
schema.pre<Model>('update', (next, doc: any) => {
    next()
})

export const Rate = connection.model<Model>('rate', schema);
