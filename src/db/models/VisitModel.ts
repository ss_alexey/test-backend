import { Document, Schema } from 'mongoose';
import { VisitParams } from '../../interfaces';
import { connection } from '../../server';

type Model = Document &
  VisitParams & {
    createdAt: Date;
  };

const schema: Schema<Model> = new Schema<Model>(
  {
    userId: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User',
      index: true
    },
    establishmentId: {
      required: true,
      type: Schema.Types.ObjectId,
      refPath: 'onModel',
      index: true
    },
    onModel: {
      type: String,
      required: true,
      enum: ['establishment']
    },
    createdAt: Date,
    testName: String
  },
  {
    toJSON: {
      transform: (doc, ret) => {
        ret.establishmentId = ret.establishmentId.toJSON();
        ret.userId = ret.userId.toJSON();
        ret.id = ret._id.toJSON();
        delete ret.onModel;
        delete ret._id;
        delete ret.__v;
      }
    }
  }
);
schema.pre<Model>('save', function(next, done: any) {
  if (!this.createdAt) {
    this.createdAt = new Date();
  }
  next();
});

export const Visit = connection.model<Model>('visit', schema);
