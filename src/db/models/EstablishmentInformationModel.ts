import { Document, Schema } from 'mongoose';

import { EstablishmentInformation } from '../../interfaces'
import { connection } from '../../server'

import { getIfOpened } from '../../helpers'

import { randobBoolean } from '../db-services'

interface WorkTime {
    day_code: 1 | 2 | 3 | 4 | 5 | 6 | 7,
    open_time: string,
    close_time: string,
    day_off: boolean,
}

const addWorkTimeField = () => {
    const voteFields: { [key: string]: any }
        = {}
    for (let i = 0; i <= 6; i++) {
        voteFields[i] = { open_time: { hour: Number, minute: Number }, close_time: { hour: Number, minute: Number }, day_off: Boolean }
    }
    return voteFields
}


type Model = Document & EstablishmentInformation
const schema: Schema<Model> = new Schema<Model>(
    {
        opened: Boolean,
        workTime: addWorkTimeField(),
        exceptionWorkTime: Object,
        feature: [{ value: { type: Schema.Types.ObjectId, ref: 'establishmentFeature' }, favorite: Boolean }],
        target: { type: Schema.Types.ObjectId, ref: 'establishment' }
    },
    {
        toJSON: {
            virtuals: true,
            transform: (doc, ret) => {
                ret.feature = ret.feature.map((item: any) => {
                    const { _id, ...other } = item
                    return other
                })
                ret.opened = false;
                ret.target = ret.target.toJSON()
                ret.id = ret._id.toJSON();
                delete ret._id
                delete ret.__v;
            }
        }
    }
);

schema.pre<Model>('save', async (next, done: any) => {
    if (!this.feature) {
        this.feature = {
            selected: [],
            all: []
        }
    }
    next();

});

export const EstablishmentInformationModel = connection.model<Model>('establishmentInformation', schema)

EstablishmentInformationModel.find().exec().then((res: any) => {
   

    res.forEach((inf: any) => {
       const newFeature = inf.feature.filter((f: any) => {
            if (f.favorite === true) {
                return true
            } else {
                return false
            }
        })
        //    const a = newFeature.map((f: any) => {
        //         return { ...f, favorite: randobBoolean() }
        //     })
        //     console.log(a);
        // console.log(res.id);
        console.log(inf._id);
        
        EstablishmentInformationModel.findByIdAndUpdate(inf._id, { $set: { feature: newFeature } }).exec()
    })
    console.log(res._id);
    //  let arr: string[] = []
    //  let newFeature = []
    // res.forEach((r: any) => {
    //     console.log(r.feature);
    //     let arr: string[] = []
    //     if (arr.includes(r.feature.value)) {

    //     } else {
    //         arr.push(r.feature.value)
    //     }

    // })
    //   res.feature.forEach((element: any) => {
    //       console.log(element);

    //   });
})