import { SaveRate } from "@interfaces";
import { Rate, Establishment } from '../../models'
import * as Boom from 'boom'



export const saveRateDb = async (rate: SaveRate) => {
    try {
        if (rate.value < 1 || rate.value > 10) {
            throw Boom.badData()
        }
        Rate.findByIdAndUpdate(rate.target, { $push: { [`votes.${rate.value}`]: rate.owner } }, async (err, doc: any) => {
            if (err) {
                throw err
            }
            let total = 1
            let weights = rate.value
            const keys = Object.keys(doc.votes)
            keys.forEach(key => {
                if (key !== '$init') {
                    total += doc.votes[key].length
                    weights += doc.votes[key].length * +key
                }
            })
            await Establishment.findByIdAndUpdate(doc.target, {
                $set: {
                    'rating.average': (weights / total).toFixed(1),
                    'rating.voteCount': total,
                    'rating.lastVote': Date.now()
                }
            })
        })

        return {success: true}
    } catch (error) {
        throw error
    }
}