import { EstablishmentRegister, UserParams, EstablishmentParams } from '../../../interfaces';
import { Establishment, Rate } from '../../models';
import { EstablishmentDetailsModel, EstablishmentInformationModel, EstablishmentFeatureModel } from '../../../db/models';

const setDefaultWorkTime = () => {
    const voteFields: { [key: string]: any }
        = {}
    for (let i = 1; i <= 7; i++) {
        voteFields[i] = { open_time: { hour: 9, minute: 0 }, close_time: { hour: 22, minute: 0 }, day_off: false }
        if (i === 6 || i === 7) {
            voteFields[i] = { open_time: { hour: 9, minute: 0 }, close_time: { hour: 22, minute: 0 }, day_off: true }
        }
    }
    return voteFields
}
export const randobBoolean = () => {
    return !!(Math.floor(Math.random() * (2 - 0)) + 0)
}

const randomNumber = (max: number) => {
    return Math.floor(Math.random() * (max - 0)) + 0;
}

const randomFeature = async () => {
    const arr: any[] = []
    const result = await EstablishmentFeatureModel.find()
    const fetureCount = randomNumber(result.length)
    const setRandomFeatue = () => {
        const index = randomNumber(result.length)
        return { value: result[index].id, favorite: randobBoolean() }
    }
    const addFeature = () => {
        const feature = setRandomFeatue()
        if (arr.includes(feature)) {
            addFeature()
        } else {
            arr.push(feature)
        }
    }
    for (let i = 0; i <= fetureCount; i++) {
        addFeature()
    }
    return arr

}
randomFeature()

export const saveEstablishment = async (est: EstablishmentRegister) => {
    try {
        const features = await randomFeature()
        const ratingModelId = (await new Rate().save()).id
        const detailsModeId = (await new EstablishmentDetailsModel().save()).id
        const informationModelId = (await new EstablishmentInformationModel({ workTime: setDefaultWorkTime(), exceptionWorkTime: { a: 'asd' }, feature: features }).save()).id
        const newEst: EstablishmentParams = {
            ...est,
            rating: {
                lastVote: null,
                modelId: ratingModelId,
                voteCount: 0,
                average: 0
            },
            id: null,
            details: detailsModeId,
            information: informationModelId
        }
        const newEstId = (await new Establishment(newEst).save())._id
        await EstablishmentInformationModel.findByIdAndUpdate(informationModelId, { $set: { target: newEstId } })
        await Rate.findByIdAndUpdate(ratingModelId, { $set: { target: newEstId } })
        await EstablishmentDetailsModel.findByIdAndUpdate(detailsModeId, { $set: { establishment: newEstId } })
        return { res: true };
    } catch (error) {
        console.log(error);

        throw error;
    }
};

interface SearchQuery {
    type?: string[],
    minRate?: number
}



export const getEstablishments = async (userId?: string, limit?: number | null, query?: SearchQuery) => {
    // if (est.type !== 'coffee' && est.type !== 'fast_food' && est.type !== 'restaraunt' && est.type !== 'shushi_bar') 
    // const qArr = ['shushi_bar', 'coffee', 'fast_food']
    // if (!query) {
    //     query = {
    //     }
    // }
    // // age: { $gt: 17, $lt: 66 },
    // query.type = qArr
    // const q: any = {
    // }
    // if (query.type) {
    //     q.type = { $in: query.type }
    //     q['rating.average'] = { $gt: 3.5 }
    // }
    // console.log(q);
    // {
    //     near: { type: "Point", coordinates: 
    //     [  
    //       35.257658,

    //       47.828353
    //       ] },
    //       distanceField: "dist",
    //       maxDistance: 1000,
    //       distanceMultiplier:0.001,
    //       num: 1,
    //       spherical: true
    // }
    // $geoNear: {
    //     near: { type:"Point", '$locations.latlong': geo.ll },
    //     maxDistance: 40000,
    //     distanceField: "dist.calculated"
    //  }
    // "$nearSphere": {
    //     "$geometry": {
    //         "type": "Point",
    //         "coordinates": [parseFloat(req.params.lng), parseFloat(req.params.lat)] 
    //     },
    //     "$maxDistance": distanceInMeters
    // },
    // "loc.type": "Point"
    try {
        //  const result =   (await Establishment.find().where('location'))
        //  .near({
        //         center: [35.257658, 47.828353],
        //         distanceField: "dist.calculated", // required
        //         maxDistance: 0.008,
        //         query: { type: "public" },
        //         includeLocs: "dist.location",
        //         uniqueDocs: true,
        //         num: 5
        //     });
        //    const a = Establishment.aggregate().near( {
        //         near: { type: "Point", coordinates: 
        //         [  
        //           35.257658,

        //           47.828353
        //           ] },
        //           distanceField: "location.dist",
        //           maxDistance: 1000,
        //           distanceMultiplier:0.001,
        //           num: 1,
        //           spherical: true
        //     })
        // const test = (await a.limit(5)).map(res => {
        //     console.log(res);

        //     // res = res.toJSON()
        //     return res.location
        // })
        // console.log(test);
        // return test
        // await Establishment.find({
        //     location: {
        // $geoNear: {
        //     // $spherical: true,
        //     // $geometry: {
        //     //     type: 'Point',
        //     //     coordinates: [35.257658, 47.828353],
        //     //     distanceField: "dist.calculated",
        //     //     spherical: true
        //     // },
        //     // $maxDistance: 1000,
        //             near: {
        //                 type: "Point", coordinates:
        //                     [
        //                         35.257658,

        //                         47.828353
        //                     ]
        //             },
        //             distanceField: "location.dist",
        //             maxDistance: 1000,
        //             distanceMultiplier: 0.001,
        //             num: 1,
        //             spherical: true
        //         },


        //     }
        // })
        // console.log(result);

        // return result
        // Establishment.find()
        const agregations: any[] = []
        agregations.push({
            $geoNear: {
                near: {
                    type: "Point", coordinates:
                        [
                            35.257658,

                            47.828353
                        ]
                },
                distanceField: "location.dist",
                maxDistance: 1000,
                distanceMultiplier: 0.001,
                num: 1,
                spherical: true

            }
        })
        // agregations.push({
        //     $lookup: {
        //         from: 'establishmentdetails',
        //         localField: 'details',
        //         foreignField: '_id',
        //         as: 'details'
        //     }
        // })
// const a = (await Establishment.find().where('location').near().)
        // const a = (await Establishment.aggregate(agregations).exec())
      
    
        return (await Establishment.find()
            .sort({ 'rating.average': -1 })
            .skip(0)
            .limit(limit)
            .populate('details')
            .populate({ path: 'comments', populate: { path: 'owner' }, options: { limit: 5 } })
            .populate({ path: 'currentUserVisit', match: { userId: userId } })
            .populate({ path: 'information', populate: { path: 'feature.value' } })
            .populate({ path: 'places', populate: { path: 'tables' } })
            .populate('freePlaces')
            // .populate({ path: 'information', populate: { path: 'feature.all' } })
        ).map(res => {
            const result = res.toJSON()
            // console.log(result.freePlaces);
            if (!userId) {
                const { currentUserVisit, ...other } = result
                return other
            }
            return result
        })

    } catch (error) {
        throw error;
    }
};
export const getDbEstablishmentById = async (id: string) => {
    try {
        return (await Establishment.findById(id)
            .populate({ path: 'comments', populate: { path: 'owner', populate: { path: 'visit', match: { establishmentId: id } } } })
            .populate({ path: 'comments', populate: { path: 'like', match: { value: true } } })
            .populate({ path: 'comments', populate: { path: 'dislike', match: { value: false } } })
            .populate({ path: 'information', populate: { path: 'feature.value' } })
            .populate({ path: 'details', populate: { path: 'userPhotos.owner', select: { 'avatar': 1, 'firstName': 1 } } })
            .populate({ path: 'rating.modelId' })
            .populate({ path: 'places', populate: { path: 'tables' } })
            .populate('freePlaces')
        )

            .toJSON();


    } catch (error) {
        throw error;
    }
};
export const getEstablishmentById = async (id: string) => {
    try {
        const result = await Establishment.findById(id)
        return result
    } catch (error) {
        throw error
    }
}