
import { DishCatergoryRegister } from '@interfaces'
import { Category } from '../../../../models'
// import { DishCategory } from '../../../../connections'

export const saveDishCategoryToDb = async (category: DishCatergoryRegister) => {
    try {
        return (await new Category(category).save())
    } catch (error) {
        throw error
    }
}

