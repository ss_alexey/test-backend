import { DishRegister } from "../../../../../interfaces";
import { Dish, Category } from '../../../../models'

export const saveDishToDb = async (dish: DishRegister) => {
    try {
        const result = await new Dish(dish).save()
        await Category.findByIdAndUpdate(dish.category, { $push: { dishes: result.id } })
        return result
    } catch (error) {
        throw error
    }
}
export const findDishByName = async (name: string) => {
    try {
        return (await Dish.findOne({ name: name }))
    } catch (error) {
        throw error
    }
}
export const findDishById = async (id: string) => {
    try {
        return (await Dish.findById(id))
    } catch (error) {
        throw error
    }
}