import { AddEstablishmentPhoto } from '@interfaces'
import { EstablishmentDetailsModel } from '../../../models'
export const addEstablishmentPhoto = async (newPhoto: AddEstablishmentPhoto) => {
    const result = await EstablishmentDetailsModel.findByIdAndUpdate(newPhoto.establishmentDetailModelId, { $push: { photos: newPhoto.imgUrl } })
   
    return result
}