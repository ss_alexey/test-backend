import { AddEstablishmentUserPhoto } from '@interfaces'

import { EstablishmentDetailsModel } from '../../../models'


export const addEstablishmentUserPhotoDb = async (newPhoto: AddEstablishmentUserPhoto) => {
    try {
        const result = await EstablishmentDetailsModel.findByIdAndUpdate(newPhoto.estModelDetalId, { $push: { userPhotos: { owner: newPhoto.userId, imgUrl: newPhoto.imgUrl } } })
        return result
    } catch (error) {
        throw error
    }

}
