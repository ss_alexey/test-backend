import { VisitRegister } from "@interfaces";
import { Visit } from '../../models'

export const saveVisit = async (visit: VisitRegister) => {
    try {
        return (await new Visit({ ...visit, onModel: 'establishment' }).save())
    } catch (error) {
        throw error
    }
}