import { User } from '../../models';
import {ChangeUserAvatar} from '@interfaces'


export const changeUserAvatar = async (newPhoto: ChangeUserAvatar) => {
    try {
        const result = await User.findByIdAndUpdate(newPhoto.userId, { $set: { avatar: newPhoto.imgUrl } })
        return result
    } catch (error) {
        throw error
    }
}