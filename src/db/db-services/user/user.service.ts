import { User } from '../../models';
// import { RegisterUser, GoogleUserRegister, UserParamsJson } from '../../../interfaces'
import { RegisterUser } from '@interfaces'
import * as Boom from 'boom'


export const saveUser = async (user: RegisterUser | any) => {   
    try {
        return (await new User(user).save())
    } catch (error) {  
        throw error
    }
}
export const findUserById = async (id: string) => {
    try {
        const result = await User.findById(id)
        return result
    } catch (error) {
        throw error
    }
}

// export const getUserById = async (id: string): Promise<any> => {
//     try {
//         return await User.findById(id).then(res => res.toJSON())
//     } catch (error) {
//         throw error
//     }
// }
// export const registerUser = async (user: RegisterUser | GoogleUserRegister) => {
//     try {
//         return new User(user).save()
//     } catch (e) {
//         throw Boom.badData()
//     }
// }
export const findUserByEmail = async (email: string) => {
    try {
        return (await User.findOne({ email: email }))
    } catch (error) {
        throw error
    }
}
export const findUserByPhone = async (phone: string) => {
    try {
        return (await User.findOne({ phone: phone }))
    } catch (error) {
        throw error
    }
}
// export const findUserIdByEmail = async (email: string): Promise<string> => {
//     try {
//         return (await User.findOne({ email: email })).id
//     } catch (error) {
//         throw error
//     }
// }
export const updateUserLastVisit = async (id: string) => {
    try {
        await User.findByIdAndUpdate(id, { lastVisit: new Date() })
    } catch (error) {
        throw error
    }
}


