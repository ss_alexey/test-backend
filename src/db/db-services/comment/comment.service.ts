import { Comment, Establishment } from '../../models'
import { RegisterComment } from '../../../interfaces'

// type Model = 'establishment' | 'comment'

export const saveComment = async (comment: RegisterComment) => {
    try {
        const result = await new Comment(comment).save()
        // await Establishment.findByIdAndUpdate(comment.parent, { $push: { comments: (await new Comment(comment).save()).id } })
        return result.toJSON()

    } catch (error) {
        throw error
    }
}
export const getCommentById = async (id: string) => {
    try {
        const result = await Comment.findById(id)
        return result
    } catch (error) {
        throw error
    }
}