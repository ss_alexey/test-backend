import { File } from '../../connections'
import { UploadFile } from '../../../interfaces';

export const saveFileToDb = async (file: UploadFile) => {
    try {
        const newFile = await new File({ file: file.buffer }).save()
        return newFile
    } catch (error) {
        throw error
    }

}