import { SaveLike } from '@interfaces'
import { Like } from '../../models'


export const saveLikeDb = async (like: SaveLike) => {
    try {
        const result = await new Like(like).save() 
        return result
    } catch (error) {
        throw error
    }
}