import 'reflect-metadata'; // this shim is required
import * as supertest from 'supertest';
import { startServer } from '../server';
import MongodbMemoryServer from 'mongodb-memory-server';


import { regiterUser, loginUser } from './route/authRoute'
import { registerRestouran } from './route/restourant'
import { reviewTest } from './reviews'

const testDbUrl = async () => {
  const mongod = new MongodbMemoryServer()
  return mongod.getConnectionString()
}
export let request: supertest.SuperTest<supertest.Test>
beforeAll(async () => {
  const port = 3012;
  const url = await testDbUrl();
  const app = await startServer(port, url);
  request = await supertest(app);
}, 600000);
afterAll(() => setTimeout(() => {
  process.exit()
}, 1000));


describe('auth route test', () => {
  regiterUser()
  loginUser()
})
describe('establishment route test', () => {
  registerRestouran()
})
describe('testing visits, comments, likes and rates', () => {
  reviewTest()
})

