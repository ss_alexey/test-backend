import { request } from '../user.routes.test';

export const reviewTest = async () => {
    test('test reviews', async () => {
        await request.get('/establishment').then(async estList => {
            const estId = estList.body.data[0].id
            const estParamsId = estList.body.data[0].details.id
            const estRateModelId = estList.body.data[0].rating.modelId
            console.log('MODEL', estRateModelId);

            const userToken = (await request.post('/auth/login').send({
                password: "password",
                phone: "+380666343172"
            })).body.data.token

            await expect(estList).toBeDefined()
            await expect(estList.status).toBe(200)

            const commentId = (await request.post('/comment')
                .set('Authorization', userToken)
                .send({ text: 'any text for comment', parent: estId })
                .then(res => {
                    expect(res.body.parent).toBe(estId)
                    return res
                })).body.id

            await request.post('/like')
                .set('Authorization', userToken)
                .send({ value: true, parent: commentId })
                .then(res => {
                    expect(res).toBeDefined()
                })
            await request.post('/like')
                .send({ value: true, parent: commentId })
                .catch(error => {
                    expect(error.status).toBe(401)
                })
            await request.post('/comment')
                .send({ text: 'any text for comment', parent: estId })
                .catch(error => {
                    expect(error.status).toBe(401)
                })
            await request.post('/rate')
                .send({ target: estId, value: 1 })
                .catch(error => {
                    expect(error.status).toBe(401)
                })
            await request.post('/rate')
                .set('Authorization', userToken)
                .send({ target: estId, value: 1 })
                .catch(error => {
                    expect(error.status).toBe(422)
                })
            await request.post('/rate')
                .set('Authorization', userToken)
                .send({ target: estRateModelId, value: 3 })
                .catch(error => {
                    expect(error).toBeUndefined()
                }).then(res => {
                    expect(res).toBeDefined()
                })
            await request.get(`/establishment/${estId}`).then(res => {
                const rating = res.body.data.rating
                expect(rating.voteCount).toBe(1)
                expect(rating.average).toBe(3)
            }).catch(error => {
                expect(error).toBeUndefined()
            })
            await request.post('/rate')
                .set('Authorization', userToken)
                .send({ target: estRateModelId, value: 4 })
                .catch(error => {
                    expect(error).toBeUndefined()
                }).then(res => {
                    expect(res).toBeDefined()
                })
            await request.post('/rate')
                .set('Authorization', userToken)
                .send({ target: estRateModelId, value: 6 })
                .catch(error => {
                    expect(error.response.status).toBe(422)
                }).then(res => {
                    expect(res).toBeUndefined()
                })
            await request.get(`/establishment/${estId}`).then(res => {
                const comment = res.body.data.comments[0]
                expect(comment.like).toBe(1)
                expect(comment.dislike).toBe(0)
                const rating = res.body.data.rating
                expect(rating.voteCount).toBe(2)
                expect(rating.average).toBe(3.5)
            })
            // await request.get(`/establishment/${estId}`).then(res => {
            //     console.log(res.body.data.rating);
            // })
        })
    })
}
