import { request } from '../../user.routes.test';

export const regiterUser = async () => {
    const user = {
        firstName: "firstName",
        password: "password",
        confirmPassword: "password",
        phone: "+380666343172"
    }
        test('register user', async () => {
            await request.post('/auth/register').send(user).catch((e: any) => {
                expect(e.status).toBe(406)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
            await request.post('/auth/register').send({ ...user, confirmPassword: 'passworda' }).catch((e: any) => {
                expect(e.status).toBe(409)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
            await request.post('/auth/register').send(user).catch((e: any) => {
                expect(e.status).toBe(409)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
            await request.post('/auth/register').send({ ...user, email: 'asdasd' }).catch((e: any) => {
                expect(e.status).toBe(409)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
            await request.post('/auth/register').send({ ...user, email: 'some@email.com' }).catch((e: any) => {
                expect(e.status).toBe(409)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
            await request.post('/auth/register').send({ ...user, email: 'some@email.com' }).catch((e: any) => {
                expect(e.status).toBe(409)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
            await request.post('/auth/register').send({ ...user, firstName: null }).catch((e: any) => {
                expect(e.status).toBe(409)
            }).then((res: any) => {
                if (res) {
                    expect(res.status).toBe(200)
                }
            })
        })
}
