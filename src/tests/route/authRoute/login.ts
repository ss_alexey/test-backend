import { request } from '../../user.routes.test';

export const loginUser = async () => {
    const user = {
        password: "password",
        phone: "+380666343172"
    }
    test('login user', async () => {
        await request.post('/auth/login').send(user).catch((e: any) => {
            expect(e.status).toBe(406)
        }).then((res: any) => {
            if (res) { 
                expect(res.status).toBe(200)
                expect(res.body.data.token).toBeTruthy()
            }
        })
        await request.post('/auth/login').send({...user, password: 'pass'}).catch((e: any) => {
            expect(e.status).toBe(406)
        }).then((res: any) => {
            if (res) {
                expect(res.status).toBe(200)
            }
        })
        await request.post('/auth/login').send({...user, phone: "+380666343171"}).catch((e: any) => {
            expect(e.status).toBe(404)
        }).then((res: any) => {
            if (res) {
                expect(res.status).toBe(200)
            }
        })
    })
}
