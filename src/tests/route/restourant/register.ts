import { request } from '../../user.routes.test';

// {
//     "title": "Coffee bar",
//       "type": "coffee",
//       "adress": {
//           "number": 33,
//           "street": "srteet",
//           "city": "city",
//           "country": "country"
//       },
//       "phone": "+380665349846"}
export const registerRestouran = async () => {
    const rest = {
        title: "Coffee bar",
        type: "coffee",
        adress: {
            number: 10,
            street: 'street',
            city: 'city',
            country: 'country'
        },
        phone: '+380665349846'
    }

    test('restourant register', async () => {
        const filePath = `${__dirname}/test.jpg`
        await request.post('/establishment').send({ body: JSON.stringify(rest) }).catch((e: any) => {
            expect(e.status).toBe(406)
            expect(e.response.body.message).toBe('image required')
        }).then((res: any) => {
            if (res) {
                expect(res.status).toBe(200)
            }
        })
        await request.post('/establishment').field({ body: JSON.stringify(rest) }).attach('file', filePath).catch((e: any) => {
            expect(e).toBeUndefined()
        }).then((res: any) => {
            if (res) {
                expect(res.status).toBe(200)
            }
        })

    })
}
