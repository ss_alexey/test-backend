import { Validator, validate, ValidationError, IsAlpha, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, Equals, IsPhoneNumber, IsMobilePhone, IsNotEmpty, IsString } from "class-validator";
import * as Boom from 'boom'

import { DishCatergoryRegister } from '../../../../interfaces'

import { findUserByEmail, findUserByPhone } from '../../../../db/db-services'
import { validateClass } from '../../../classValidator'

class DishCategoryRegisterValidator implements DishCatergoryRegister {
    @IsString()
    @IsNotEmpty()
    title: string
}
export const validateDishCategoryRegister = async (category: DishCatergoryRegister) => {
    const validation = Object.assign(new DishCategoryRegisterValidator(), category)
    try {
        await validateClass(validation)
        return true
    } catch (error) {
        throw error
    }
}


