import { Validator, validate, ValidationError, IsAlpha, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, Equals, IsPhoneNumber, IsMobilePhone, IsNotEmpty, IsString, IsMongoId, IsNumber } from "class-validator";
import * as Boom from 'boom'

import { DishRegister } from '../../../../interfaces'

import { findDishByName } from '../../../../db/db-services'
import { validateClass } from '../../../classValidator'

class DishRegisterValidator implements DishRegister {

    description: string
    @IsString()
    @IsNotEmpty()
    name: string

    @IsMongoId()
    @IsNotEmpty()
    category: string

    @IsNumber()
    @IsNotEmpty()
    price: number
}

export const validateDishRegister = async (dish: DishRegister) => {
    if (await findDishByName(dish.name)) {
        throw Boom.conflict('already exist')
    }
    const validation = Object.assign(new DishRegisterValidator(), dish)
    try {
        await validateClass(validation)
        return true
    } catch (error) {
        throw error
    }
}


