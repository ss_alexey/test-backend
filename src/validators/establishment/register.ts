import { Validator, validate, ValidationError, IsAlpha, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, Equals, IsPhoneNumber, IsMobilePhone, IsNotEmpty, IsString, IsOptional, IsNumber, ValidateNested, IsDefined, IsMongoId } from "class-validator";
import * as Boom from 'boom'

import { EstablishmentRegister, EstablishmentAdress, EstablishmentCoordinate } from '../../interfaces'
import { validateClass } from '../classValidator'

class AdressValidator implements EstablishmentAdress {
    @IsNotEmpty()
    @IsNumber()
    number: number;
    @IsNotEmpty()
    street: string;
    @IsNotEmpty()
    city: string;
    @IsNotEmpty()
    country: string
}

class CoordinateValidator implements EstablishmentCoordinate {

    latitude: number
    longitude: number
}

class EstablishmentRegisterValidator implements EstablishmentRegister {
    details: string
    
    // @IsNotEmpty()
    coordinate:EstablishmentCoordinate

    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    type: String;

    @IsNotEmpty()
    adress: EstablishmentAdress;

    @IsNotEmpty()
    image: string

    @IsNotEmpty()
    @IsPhoneNumber('UA')
    phone: string
}

export const validateEstablishmentRegister = async (establish: EstablishmentRegister) => {
    try {
        if (typeof establish.adress !== 'object') {
            throw Boom.badData('adress is not an object')
        }
        // if (typeof establish.coordinate !== 'object') {
        //     throw Boom.badData('coordinate is not an object')
        // }
        // await validateClass(Object.assign(new CoordinateValidator(), establish.coordinate))
        await validateClass(Object.assign(new AdressValidator(), establish.adress))
        await validateClass(Object.assign(new EstablishmentRegisterValidator(), establish))
        return true
    } catch (error) {
        throw error
    }
}
// const validateClass = async (object: Object) => {
//     return validate(object).then((errors: ValidationError[]) => {
//         if (errors.length > 0) {
//             throw Boom.notAcceptable(Object.values(errors[0].constraints)[0], errors[0].constraints)
//         }
//         else {
//             return true
//         }
//     })
// }