import { IsArray, IsString, IsMongoId } from "class-validator";
import * as Boom from 'boom'

import { EstablishmentRegister, EstablishmentAdress, EstablishmentDetails } from '../../interfaces'
import { validateClass } from '../classValidator'



class EstablishmentEditValidator implements EstablishmentDetails {
userPhotos: any[]
    @IsArray()
    photos: string[]
    @IsString()
    description: string
    // @IsMongoId()
    id: string
    @IsArray()
    menu: any[]
}

export const validateEstablishmentUpdate = async (establish: EstablishmentDetails) => {
    try {
        await validateClass(Object.assign(new EstablishmentEditValidator(), establish))
        return true
    } catch (error) {
        throw error
    }
}
