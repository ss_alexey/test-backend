import { UploadFile } from "../../interfaces";
import * as Boom from 'boom'

export const validateFileIsImage = async (file: UploadFile) => {
    if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {  
        throw Boom.notAcceptable('just jpg and png formats')
    }
}