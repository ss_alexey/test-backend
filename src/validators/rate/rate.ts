
import { SaveRate } from '@interfaces'
import { validateClass } from '../classValidator'
import { User, Establishment, Like, Comment , Rate} from '../../db/models'


import { IsNotEmpty, IsMongoId, IsString, MinLength, IsBoolean, IsNumber } from 'class-validator';
import * as Boom from 'boom'


class RateRegisterValidator implements SaveRate {

    @IsMongoId()
    @IsNotEmpty()
    owner: string;

    @IsMongoId()
    @IsNotEmpty()
    target: string

    @IsNumber()
    @IsNotEmpty()
    value: number
}

export const validateRateRegister = async (rate: SaveRate): Promise<any> => {

    try {
        await validateClass(Object.assign(new RateRegisterValidator(), rate))
        if (!await User.findById(rate.owner) || !await Rate.findById(rate.target)) {
            throw Boom.badData('no such record')
        }
    } catch (error) {
        throw error
    }

}