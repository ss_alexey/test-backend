import { AuthService } from '../../helpers'
import { RegisterComment } from '../../interfaces'
import { validateClass } from '../classValidator'

import { getCommentById, getEstablishmentById, findUserById } from '../../db/db-services'

import * as Boom from 'boom'
import { IsNotEmpty, IsMongoId, IsString, MinLength, IsBoolean, validate, ValidationError } from 'class-validator';
type Roles = 'text' | 'like';
type CommentValidationGroup = Roles[];



class CommentRegisterValidator implements RegisterComment {
  @IsMongoId()
  @IsNotEmpty()
  owner: string
  @IsString()
  @IsNotEmpty()
  @MinLength(10)
  text: string;

  @IsMongoId({ always: true })
  @IsNotEmpty({ always: true })
  parent: string
}

export const validateCommentRegister = async (comment: RegisterComment): Promise<any> => {
  try {  
    if (!(await getEstablishmentById(comment.parent))) {
      throw Boom.notAcceptable('no such establishment')
    }

    return validate(Object.assign(new CommentRegisterValidator(), comment)).then((errors: ValidationError[]) => {
      if (errors.length > 0) {
        throw Boom.notAcceptable(
          Object.values(errors[0].constraints)[0],
          errors[0].constraints
        );
      } else {
        return true;
      }
    });
  } catch (error) {
    throw error
  }

}