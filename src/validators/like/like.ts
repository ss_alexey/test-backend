
import { SaveLike } from '../../interfaces'
import { validateClass } from '../classValidator'
import { User, Establishment, Like, Comment } from '../../db/models'


import { IsNotEmpty, IsMongoId, IsString, MinLength, IsBoolean } from 'class-validator';
import * as Boom from 'boom'


class LikeRegisterValidator implements SaveLike {

    @IsMongoId()
    @IsNotEmpty()
    owner: string;

    @IsMongoId()
    @IsNotEmpty()
    parent: string

    @IsBoolean()
    value: boolean
}

export const validateLikeRegister = async (like: SaveLike): Promise<any> => {

    try {
        await validateClass(Object.assign(new LikeRegisterValidator(), like))
        if (!await User.findById(like.owner) || !await Comment.findById(like.parent)) {
            throw Boom.badData('no such record')
        }
    } catch (error) {
        throw error
    }

}