import { validate, ValidationError } from "class-validator";
import * as Boom from 'boom'

export const validateClass = async (object: Object) => {
    return validate(object).then((errors: ValidationError[]) => {
        if (errors.length > 0) {
            throw Boom.notAcceptable(Object.values(errors[0].constraints)[0], errors[0].constraints)
        }
        else {
            return true
        }
    })
}