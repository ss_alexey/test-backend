import {
  validate,
  ValidationError,
  IsAlpha,
  Length,
  IsNotEmpty,
} from 'class-validator';
import * as Boom from 'boom';

import { RegisterUser } from '../../interfaces';
type Roles = 'apiUser' | 'socialUser' | 'estOwner' | 'register' | 'login';

type ValidationRoles = Roles[];
import { findUserByEmail, findUserByPhone } from '../../db/db-services';

class RegisterUserValidator implements RegisterUser {
  @IsNotEmpty({ always: true })
  @IsAlpha({ always: true })
  firstName: string;

  @IsNotEmpty({ groups: ['socialUser'] as ValidationRoles })
  email: string;

  @IsNotEmpty({
    groups: ['apiUser', 'register', 'login'] as ValidationRoles
  })
  @Length(3, 50, {
    groups: ['apiUser', 'register', 'login'] as ValidationRoles
  })
  password: string;

  @IsNotEmpty({ groups: ['apiUser', 'register'] as ValidationRoles })
  confirmPassword: string;

  @IsNotEmpty({ groups: ['apiUser'] as ValidationRoles })
  phone: string;
}
export const validateUserRegister = async (
  user: RegisterUser,
  groups: ValidationRoles
) => {
  if (user.email && await findUserByEmail(user.email) || user.phone && await findUserByPhone(user.phone)) {
    throw Boom.conflict('such user already exist')
  }
  const validation = Object.assign(new RegisterUserValidator(), user);
  return validate(validation, {
    groups: groups
  }).then((errors: ValidationError[]) => {
    if (errors.length > 0) {
      throw Boom.notAcceptable(
        Object.values(errors[0].constraints)[0],
        errors[0].constraints
      );
    } else {
      return true;
    }
  });
};
