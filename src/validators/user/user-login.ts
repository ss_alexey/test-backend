import { AuthService } from '../../helpers'
import { LoginUser } from '../../interfaces'
// import { validatePhone } from './user-register'
import * as Boom from 'boom'


export const validateLogin = async (dbUser: any, user: LoginUser): Promise<string> => {
    // await validatePhone(user.phone)
    if(!user.password){
        throw Boom.notAcceptable('password required')
    }
    if (!dbUser) {
        throw Boom.notFound('no such user')
    }
    if (dbUser.password !== AuthService.getHashed(user.password)) {
        throw Boom.notAcceptable('wrong password')
    }
    return dbUser.id
}