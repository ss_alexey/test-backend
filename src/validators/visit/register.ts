
import { VisitRegister } from '../../interfaces'
import { validateClass } from '../classValidator'
import { User, Establishment } from '../../db/models'


import { IsNotEmpty, IsMongoId, IsString, MinLength } from 'class-validator';
import * as Boom from 'boom'


class VisitRegisterValidator implements VisitRegister {

    @IsMongoId()
    @IsNotEmpty()
    establishmentId: string;

    @IsMongoId()
    @IsNotEmpty()
    userId: string
}

export const validateVisitRegister = async (visit: VisitRegister): Promise<any> => {

    try {
        await validateClass(Object.assign(new VisitRegisterValidator(), visit))
        if (!await User.findById(visit.userId) || !await Establishment.findById(visit.establishmentId)) {
            throw Boom.badData('no such record')
        }
    } catch (error) {
        throw error
    }

}