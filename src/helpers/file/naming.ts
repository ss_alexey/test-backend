

import { DOMAIN, API_PORT, IMAGE_FOLDER_ROUTE, FILE, MONGO_DB_NAME } from "../../config";

interface FileNameParams {
    id: string,
    collection: string,
    label: string,
    format: string
}
export const createImageUrl = async (id: string) => {
    console.log('REMOVE PORT');
    const DOMAIN = MONGO_DB_NAME ? 'http://clubdev.ru' : 'http://localhost:3000'
    return `${DOMAIN}/image/${id}`
}
export const createUserImageUrl = async (fileName: string): Promise<string> => {
    const route = FILE.image.routePath
    const folderPath = FILE.image.folderPath
    const query = FILE.image.query
    return `${folderPath}/${fileName}`
    // return `${DOMAIN}${API_PORT || 3000}/${route}?${query}=${fileName}`
}
export const createImageFilePath = async (fileName: string): Promise<string> => {
    return `${IMAGE_FOLDER_ROUTE}/${fileName}`
}
export const createAvatarUserImageFileName = async (id: string): Promise<string> => {
    const params: FileNameParams = {
        id: id,
        collection: FILE.image.ext.collections.user,
        label: FILE.image.ext.labels.avatar,
        format: FILE.image.format
    }
    try {
        return createFileName(params)
    } catch (error) {
        throw error
    }
}
export const createOriginalUserImageFileName = async (id: string): Promise<string> => {
    const params: FileNameParams = {
        id: id,
        collection: FILE.image.ext.collections.user,
        label: FILE.image.ext.labels.original,
        format: FILE.image.format
    }
    try {
        return createFileName(params)
    } catch (error) {
        throw error
    }

}
const createFileName = async (params: FileNameParams): Promise<string> => {
    const { id, collection, label, format } = params
    try {
        return `${id}${collection}${label}${format}`
    } catch (error) {
        throw error
    }

}