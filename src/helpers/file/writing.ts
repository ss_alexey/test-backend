import * as fs from 'fs'
import * as sharp from 'sharp'
import { FILE } from '../../config';

export const writeFile = async (fileBuffer: Buffer, filePath: fs.PathLike) => {
    fs.writeFile(filePath, fileBuffer, (err) => {
        if (err) throw err
    })
}
export const writeImage = async (fileBuffer: Buffer, filePath: fs.PathLike) => {
    fs.writeFile(filePath, fileBuffer, (err) => {
        if (err) throw err
    })
}
export const writeImageAvatar = async (fileBuffer: Buffer, filePath: fs.PathLike) => {
    const { height, width } = FILE.image.size.avatar
    sharp(fileBuffer).resize(height, width).toFile(filePath as string)
}
