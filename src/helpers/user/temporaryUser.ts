
import { SMS_VALID_TIME } from '../../config'
import * as Boom from 'boom'
import { RegisterUser } from '../../interfaces';


const tempUsers = new Map()

export const updateTempUser = async (phone: string, code: string) => {
    try {
        if (!tempUsers.get(phone)) {
            throw Boom.notAcceptable()
        }
        const user = tempUsers.get(phone).user
        if (!user) {
            throw Boom.notAcceptable()
        }
        tempUsers.set(phone, { code, user })
    } catch (error) {
        throw error
    }

}

export const makeTempUser = async (user: RegisterUser, code: string) => {
    tempUsers.set(user.phone, { code, user })
    setTimeout(() => {
        tempUsers.delete(user.phone)
    }, SMS_VALID_TIME);
}
export const checkUserPin = async (phone: string, pin: string): Promise<RegisterUser> => {
    const userParams = tempUsers.get(phone)
    if (userParams) {
        if (userParams.code === pin) {
            return userParams.user
        }
        throw Boom.notAcceptable('invalid code')
    } else {
        throw Boom.resourceGone('time for code is gone')
    }
}