
import * as fs from 'fs'
import * as sharp from 'sharp'
import { DOMAIN, API_PORT, IMAGE_FOLDER_ROUTE, DEFAULT_IMAGE_FORMAT, FILE } from '../config'
// interface FileNameParams {
//     id: string,
//     collection: string,
//     label: string,
//     format: string
// }
// export const createUserImageUrl = async (fileName: string): Promise<string> => {
//     const route = FILE.image.routePath
//     const query = FILE.image.query
//     return `${DOMAIN}${API_PORT || 3000}/${route}?${query}=${fileName}`
// }
// export const createImageFilePath = async (fileName: string): Promise<string> => {
//     return `${IMAGE_FOLDER_ROUTE}/${fileName}`
// }
// export const createOriginalUserImageFileName = async (id: string): Promise<string> => {
//     const params: FileNameParams = {
//         id: id,
//         collection: FILE.image.ext.collections.user,
//         label: FILE.image.ext.labels.original,
//         format: FILE.image.format
//     }
//     try {
//         return createFileName(params)
//     } catch (error) {
//         throw error
//     }

// }

// const createFileName = async (params: FileNameParams): Promise<string> => {
//     const { id, collection, label, format } = params
//     try {
//         return `${id}${collection}${label}${format}`
//     } catch (error) {
//         throw error
//     }

// }
// export const createAvatarUserImageFileName = async (id: string): Promise<string> => {
//     const params: FileNameParams = {
//         id: id,
//         collection: FILE.image.ext.collections.user,
//         label: FILE.image.ext.labels.avatar,
//         format: FILE.image.format
//     }
//     return createFileName(params)
// }
// export const writeFile = async (fileBuffer: Buffer, filePath: fs.PathLike) => {
//     fs.writeFile(filePath, fileBuffer, (err) => {
//         if (err) throw err
//     })
// }
// export const writeFileWithResise = async (fileBuffer: Buffer, folderPath: fs.PathLike, fileName: string, size: { height: number, width: number }): Promise<string> => {
//     // const filePath = `${folderPath}/${fileName}`
//     // await sharp(fileBuffer).resize(height, width).toBuffer().then(res => {
//     //     fs.writeFile(filePath, res, (err) => {
//     //         if (err) throw err
//     //     })
//     // })
//     return `${DOMAIN}${API_PORT || 3000}/image?image=${fileName}`
// }