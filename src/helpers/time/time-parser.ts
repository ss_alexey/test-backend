export const parseTimeStamp = (UNIX_timestamp: number) => {

    const a = new Date(UNIX_timestamp);
    // console.log(a);

    // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const year = a.getFullYear();
    const month = a.getMonth();
    const day = a.getDate();
    const weekday = a.getDay()
    const hour = a.getHours();
    const min = a.getMinutes();
    const sec = a.getSeconds();

    // const time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return { year, month, day, weekday, hour, min, sec }
}