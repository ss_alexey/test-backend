

interface WorkTime {
    [key: string]: {
        open_time: { hour: number, minute: number },
        close_time: { hour: number, minute: number },
        day_off: boolean,
    }

}
interface ExceptionWorkTime {

    date: { day: number, month: number, year: number },
    open_time: { hour: number, minute: number },
    close_time: { hour: number, minute: number },
    day_off: Boolean

}
export const getIfOpened = (timeTable: WorkTime, exceptionWorkTime: ExceptionWorkTime[]) => {
    const currentTime = new Date(Date.now());
    const hour = currentTime.getHours();
    const min = currentTime.getMinutes();
    const weekday = currentTime.getDay()
    const year = currentTime.getFullYear()
    const month = currentTime.getMonth()
    console.log(currentTime.getUTCDay());
    
 
    // let newExc2 = {
    //     year: 2019,
    //     month: 2,
    //     weekday: 1
    // }
    // let excep = {
    //     [year]: {
    //         [month]: { [weekday]: 1 }
    //     }

    // }
    // const test = () => {
    //     if (excep[newExc2.year]) {
    //         console.log('YEAR EXIST');
    //         if (excep[newExc2.year][newExc2.month]) {
    //             console.log('MONTH EXIST');
    //             if (excep[newExc2.year][newExc2.month][newExc2.weekday]) {
    //                 console.log('WEEKDAY EXIST');
    //             }
    //         }
    //     }
    // }
    // test()
    // newExc2 = {
    //     year: 2019,
    //     month: 2,
    //     weekday: 2
    // }
    // test()
    // console.log(excep);

    if (timeTable && typeof timeTable[weekday].open_time === 'object') {
        const openTime: { hour: number, minute: number } = timeTable[weekday].open_time
        const closeTime: { hour: number, minute: number } = timeTable[weekday].close_time
        const openTimeWork = openTime.hour * 60 + openTime.minute
        const closeTimeWork = closeTime.hour * 60 + closeTime.minute
        const currentMinutes = hour * 60 + min
        if (currentMinutes >= openTimeWork && currentMinutes <= closeTimeWork) {
            return true
        }
    }
    return false
}