export const generatePin = async (): Promise<string> => {
    function getRandomInt() {
        return Math.floor(Math.random() * (10 - 0)) + 0;
    }
    const pin = []
    for (let i = 0; i < 4; i++) {
        pin.push(getRandomInt())
    }    
    return pin.join('')

}