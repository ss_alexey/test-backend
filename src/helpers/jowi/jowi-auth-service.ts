import * as sha from 'sha256'
import { JOWI_CONFIG } from '../../config'
const API_KEY = JOWI_CONFIG.API_KEY
const API_SECRET = JOWI_CONFIG.API_SECRET


export const createSigString = async (): Promise<string> => {
    const sha256: string = sha(API_KEY + API_SECRET)
    return sha256.substring(0, 10) + sha256.slice(-5)
    return '1213'
}