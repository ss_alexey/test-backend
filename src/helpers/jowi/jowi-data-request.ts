
import { createSigString } from './jowi-auth-service'
import * as https from 'https'
// import {} from ''
// import {} from ''
import { JOWI_CONFIG } from '../../config'
import { JoxiMenu, JowiEstablishmentResponse } from '@interfaces';

const baseUrl = JOWI_CONFIG.URL

const getData = async (req: string) => {
    const reqUrl = `${baseUrl}${req}`
    // console.log(reqUrl);
    try {
        return new Promise(function (resolve, reject) {
            const req = https.request(reqUrl, (res) => {
                if (res.statusCode < 200 || res.statusCode >= 300) {
                    throw new Error('statusCode=' + res.statusCode)
                    return reject(new Error('statusCode=' + res.statusCode));
                }
                let body: any[] = [];
                res.on('data', function (chunk) {
                    body.push(chunk);

                });
                res.on('end', function () {
                    try {
                        body = JSON.parse(Buffer.concat(body).toString());
                    } catch (e) {
                        reject(e);
                    }
                    resolve(body);
                });
                console.log(body);

            });
            req.on('error', function (err) {
                reject(err);
            });

            req.end();
        });
    } catch (error) {
        throw error
    }
}
export const getRestaurants = async (): Promise<JowiEstablishmentResponse> => {
    try {
        const query = `?api_key=${JOWI_CONFIG.API_KEY}&sig=${await createSigString()}`
        return (await getData(`/restaurants${query}`) as JowiEstablishmentResponse)
    } catch (error) {
        throw error
    }

}
export const getRestaurantById = async (id?: string) => {
    if (!id) {
        id = 'a40b4cd6-222c-4f38-a010-83e5bf9c0da1'
    }
    try {
        const query = `?api_key=${JOWI_CONFIG.API_KEY}&sig=${await createSigString()}`
        return (await getData(`/restaurants/${id}${query}`)) as JoxiMenu
    } catch (error) {
        throw error
    }
}
export const getRestTables = async (id: string) => {

    try {
        const query = `?api_key=${JOWI_CONFIG.API_KEY}&sig=${await createSigString()}`
        return (await getData(`/halls/${id}${query}`)) as JoxiMenu
    } catch (error) {
        throw error
    }
}