import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken'
import { SALT, tokenTime } from '../config'
import { GoogleUserDecoded } from '../interfaces'
import {User} from '../db/models'
import * as Boom from 'boom'

export class AuthService {
    // public static getUserFromToken = async (token: string) => {
    //     try {
    //         const userId =(jwt.verify(token, SALT) as {
    //             id: string,
    //             iat: number,
    //             exp: number
    //         }).id
    //         const user = (await User.findById(userId))
    //         return user
    //     } catch (error) {
    //         throw error
    //     }
    // }
    public static getIdFromToken = async (token: string) => {
        try {
            return (jwt.verify(token, SALT) as {
                id: string,
                iat: number,
                exp: number
            }).id
        } catch (error) {
            throw error
        }
    }
    public static decodeJwtToken(token: string): GoogleUserDecoded {
        try { return jwt.decode(token) as GoogleUserDecoded } catch (e) {
            throw e
        }
    }
    public static getHashed(string: string): string {
        return crypto.createHash('sha256').update(string).digest('hex');
    }

    public static tokenEncode(id: string): string {
        return jwt.sign({
            id
        }, SALT, { expiresIn: tokenTime });
    }
    public static tokenDecode(token: string) {
        if (!token) {
            throw Boom.unauthorized()
        }

        return jwt.verify(token, SALT) as {
            id: string,
            iat: number,
            exp: number
        }
    }
}