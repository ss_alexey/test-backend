export const hasFreePlaces = (places: any[], reserves: any[]) => {
    let ordered: any[] = []
    let free: any[] = []
    places.forEach((place: any) => {

        place.tables.forEach((table: any) => {
            if (reserves.find((reserve: any) => {
                if (reserve.tableId === table.id) {
                    ordered.push(reserve)
                    return true
                }
            })) {

            } else {
                free.push(table)
            }
        })
    })
    if (free.length > 0) {
        return true
    }
    return false
}


